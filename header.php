<?php
/**
 * The template for displaying the header
 *
 *
 * @package WordPress
 * @subpackage Keep Style
 * @since Keep Style 1.0
 */
?>
<!DOCTYPE html>
<html lang="<?= LANG_CODE ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <?php wp_head(); ?>
    <title><?= get_the_title() ?></title>
</head>
<body>

<!--<div class="preloader">-->
<!--    <div class="preloader_overlay"></div>-->
<!--    <div class="preloader_content">-->
<!--        <img src="--><?//= THEMROOT ?><!--/img/logoHeader.png" alt="Logo">-->
<!--    </div>-->
<!--</div>-->

<div class="mobile-menu">
    <?php wp_nav_menu(array(
        'theme_location'=> 'menuMobile',
    )); ?>
</div>


<!-- mobile-btn -->
	<span class='mobile-button'>
		<svg class="burger" width="30px" height="30px">
            <line class="top" type="burger" x1="0" y1="1" x2="30" y2="1" />
            <line class="middle" type="burger" x1="0" y1="11" x2="30" y2="11" />
            <line class="bottom" type="burger" x1="0" y1="22" x2="30" y2="22" />
        </svg>
	</span>
<!-- mobile-btn -->


<!-- header -->

<header class="stickyHeader">

    <div class="container">
        <div class="row">

            <nav class='hidden-xs'>
                <?php wp_nav_menu(array(
                    'theme_location'=> 'menu',
                )); ?>
            </nav>
            <a href="<?= get_home_url() ?>" class="logo">
                <img src="<?= THEMROOT ?>/img/logoHeader.png" alt="">
            </a>
            <div class="right-side">
                <div class="lang">
                    <?php keep_style_language_switcher(); ?>
                </div><!-- lang  -->
                <div class="cart hidden-xs">
                    <a href="<?= '/'.LANG_CODE.'/checkout/' ?>">корзина (<span class="count"><?= ACWProducts::getCount() ?></span>)</a>
                </div>
            </div>
        </div>
    </div>



</header>
<!-- header -->