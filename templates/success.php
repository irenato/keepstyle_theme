<?php
/*
Template Name: success
*/
?>
<?php
    unset($_SESSION['acw_file_name']);
    unset($_SESSION['acw_user_info']);
    unset($_SESSION['acw_products']);
    unset($_SESSION['acw_collect_order']);
?>


<?php get_header(); ?>
<div class="success">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <img src="<?= THEMROOT ?>/img/cartPay.png" alt="">
                <?= the_content() ?>
                <a href="<?= get_home_url() ?>">На главную</a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
