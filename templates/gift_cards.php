<?php
/*
Template Name: gift-cards
*/
?>

<?php get_header(); ?>

<div class="giftCards">
    <div class="container">
        <div class="title row">
            <div class="col-lg-12">
                Подарочные карты
                <span class="blueSmallLine"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2 col-md-5 col-md-offset-1 col-sm-6"><img src="<?= THEMROOT ?>/img/giftCard.png" alt=""></div>
            <div class="col-lg-4 col-md-5 col-sm-6">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis eros eu placerat gravida. </p>
                <p>Donec luctus vestibulum erat, id rutrum tortor. Fusce feugiat, turpis a malesuada imperdiet, erat nisi placerat ipsum, at finibus eros nibh eu quam. Quisque a risus nibh. In hac habitasse platea dictumst. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                <form class="giftForm">
                    <div class="group">
                        <div class="selectParent">
                            <span class='titleSelect'>Подарочная сумма</span>
                            <div class="resultSelect" data-value=""></div>
                            <ul class="customeSelect">
                                <li data-value="100">100 UAH</li>
                                <li data-value="200">200 UAH</li>
                                <li data-value="300">300 UAH</li>
                                <li data-value="500">500 UAH</li>
                            </ul>
                        </div>
                    </div>

                    <div class="group">
                        <input type="text" name="name" class="giftData">
                        <span class="bot-line"></span>
                        <span class="right-line"></span>
                        <label>Имя</label>
                    </div>
                    <div class="group">
                        <input type="text" name="email" class="giftData">
                        <span class="bot-line"></span>
                        <span class="right-line"></span>
                        <label>Email</label>
                    </div>

                    <textarea placeholder='Message'></textarea>
                    <input type="submit" value="add to cart">
                </form>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
