<?php
/*
Template Name: PayPal CallBack
*/
?>

<?php

if(isset($_GET['payment_status']) && isset($_GET['custom'])){

    $data = ACWOrders::getDataFromFile($_GET['custom']);
    $collect_order = $data->collect_order;
    $user_info = $data->user_info;
    $products = $data->products;
    $payment_data = sprintf('ID Транзакция: %s', $_GET['txn_id']);

    ACWOrders::createNewOrder($_GET['payment_status'], 'PayPal', $collect_order, $user_info, $payment_data, $products);

}
?>
