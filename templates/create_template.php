<?php
/*
Template Name: Create
*/
?>

<?php get_header(); ?>
<div class="constructor">



    <div class="tShirt">
        <!-- on mobile -->

        <div class="right-controll">
            <div class="mobCreateNew hidden-lg"><a href=""><span class='icon-reload'></span><i>Новая</i></a></div>

            <div class="composition">
                <div class="close_comp"><i class="icon-close"></i></div>
                <div class="composition_title"> Состав:</div>
                <p>Lorem<span>95%</span></p>
                <p>ipsum<span>20%</span></p>
                <div class="composition_title"> Уход:</div>
                <p>Только ручная стирка</p>
                <p>Стирать при температуре 30 градусов</p>
            </div>

<!--            <div class="addToBaskMob"><a href=""><i class="icon-cartAdd"></i><span>Добавить в корзину</span></a></div>-->



            <!-- on mobile -->
            <!--
                <div id="position" >
                  <label for="">Лево:<input type="number" placeholder='Лево' id='left-control'></label>
                  <br>
                  <label>Верх:<input type="number" min='0' max='100' id='top-control'></label>
                </div>  -->

            <div id="mainCanvas">
                <img src="<?= THEMROOT ?>/img/img/sweatshirt_front_dgrey.png" data-front='<?= THEMROOT ?>/img/img/sweatshirt_front_dgrey.png' data-back='<?= THEMROOT ?>/img/img/sweatshirt_back_dgrey.png' />
                <div id="" class='printArea' >
                    <canvas id="FrontCanvas" style="-webkit-user-select: none;"></canvas>
                </div>


            </div>
            <a class="flip" href="#" data-type="front"><i class='icon-flip'></i><span>Back view</span></a>
            <div class="printInformation">
                <div class="createNew">
                    <a href="" class="">Создать новую</a>
                </div>
                <div class="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                <div class="infoBtn"><i class="icon-info"></i></div>
                <div class="price"><i>65</i>$</div>
                <div class="sizeAdult">
                    <ul>
                        <li class='active'><span>s</span></li>
                        <li><span>m</span></li>
                        <li><span>l</span></li>
                        <li><span>xl</span></li>
                        <li><span>xxl</span></li>
                    </ul>
                </div>
                <div class="sizeChild">
                    <ul>
                        <li class='active'><span>62</span></li>
                        <li><span>68</span></li>
                        <li><span>74</span></li>
                        <li><span>80</span></li>
                        <li><span>86</span></li>
                        <li><span>92</span></li>
                        <li><span>98</span></li>
                        <li><span>104</span></li>
                        <li><span>110</span></li>
                        <li><span>116</span></li>
                        <li><span>122</span></li>
                        <li><span>128</span></li>
                        <li><span>134</span></li>
                        <li><span>140</span></li>
                    </ul>
                </div>
                <div class='tableSize'><a href="#">Таблица размеров</a></div>
                <div class="countTShirt">
                    <span class='sum'>Количество</span>
                    <div class='quantityTShirt'>
                        <span class="minVal">-</span>
                        <span class="counter">1</span>
                        <span class="maxVal">+</span>
                    </div>
                </div>

                <div class="composition">
                    <div class="composition_title"> <span class='active com1'>Состав:</span> <span class='com2'>Уход:</span></div>
                    <div class="composition_tab composition_tab_1">
                        <p>Lorem<span>30%</span></p>
                        <p>ipsum<span>20%</span></p>
                    </div>
                    <div class="composition_tab composition_tab_2">
                        <p>Только ручная стирка</p>
                        <p>Стирать при температуре 30 градусов</p>
                    </div>
                </div>

            </div>
            <div class="addToBasket">
                <a href=""><i class="icon-cartAdd"></i><span>Добавить в корзину</span></a>
            </div>
        </div>
        <div class="left-controll">
            <div class="wrappLeft">


                <!-- typeShirt -->
                <div class="typeShirt sameHeight">
                    <div class="title">Тип</div>
                    <div class="contentInner">
                        <ul class='listShirt'>
                            <li class='active' data-currentprice="20" data-baseprice="500" data-shirt='sweatshirt' data-name='Sweatshirt'><span class='icon-sweatshirt'></span></li>
                            <li data-shirt='shirt'  data-currentprice="15" data-baseprice="300" data-name='Shirt'><span class='icon-tshirt'></span></li>
                        </ul>

                        <div class="gender">
                            <input type="radio" name="genderMob" data-gender='adult' id="adultM">
                            <label for="adultM"><span class='icon-man'></span></label>

                            <input type="radio" name="genderMob" data-gender='child' id="child">
                            <label for="child"><span class='icon-child'></span></label>

                            <input type="radio" name="genderMob" data-gender='adult' id="adultW">
                            <label for="adultW"><span class='icon-woman'></span></label>
                        </div>
                    </div>
                </div>
                <!-- typeShirt -->

                <!-- colorShirt -->

                <div class="colorPalette sameHeight">
                    <div class="title">цвет</div>
                    <div class="contentInner">
                        <input type="radio" id="color1" name='tShirtType' data-color='grey' checked>
                        <label for="color1"><span  style='background: #9b9c9a' ></span></label>

                        <input type="radio" id="color2" name='tShirtType' data-color='maroon'>
                        <label for="color2"><span style='background: #5a050d'></span></label>

                        <input type="radio" id="color3" name='tShirtType' data-color='mint'>
                        <label for="color3"><span style='background: #00bfab'></span></label>

                        <input type="radio" id="color4" name='tShirtType' data-color='pink'>
                        <label for="color4"><span style='background: #fea4d1'></span></label>

                        <input type="radio" id="color5" name='tShirtType' data-color='red'>
                        <label for="color5"><span style='background: #ff0423'></span></label>

                        <input type="radio" id="color6" name='tShirtType' data-color='black'>
                        <label for="color6"><span style='background: #000000'></span></label>

                        <input type="radio" id="color7" name='tShirtType' data-color='white'>
                        <label for="color7"><span style='background: #ffffff'></span></label>

                        <input type="radio" id="color8" name='tShirtType' data-color='crimson'>
                        <label for="color8"><span style='background: #ff6aa0'></span></label>

                        <input type="radio" id="color9" name='tShirtType' data-color='dgrey'>
                        <label for="color9"><span style='background: #8c8e89'></span></label>
                    </div>
                </div>

                <!-- colorShirt -->


                <!-- <input type="text" id="widthPrint" style='margin-top: 50px;'> -->
                <!-- printSlides -->
                <div class="printSlides sameHeight">
                    <div class="title">Принт</div>
                    <a href="#" class="removePrint" >Удалить принт</a>

                    <div class="contentInner">
                        <img src="<?= THEMROOT ?>/img/img/svg/51wings.svg" alt="">
                        <div class="textInputCanva">
                            <div class="wrapp_text">
                                <input  class="text-string" maxlength="13" type="text" placeholder='Имя' >
                                <span class='textStringError'>A-Z</span>
                            </div>
                            <div class="wrapp_number">
                                <input  class="number-string" maxlength="2" type="text" placeholder='Номер'>
                                <span class='numberStringError'>0-9</span>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- printSlides -->



                <!-- printTexture -->
                <div class="printTexture sameHeight">
                    <div class="title">Цвет Принта</div>
                    <div class="contentInner">
                        <input type="radio" name='printTexture' id="print1" value="serebro-mat.png" >
                        <label for="print1"><span data-currentprice="10" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/serebro-mat.png)'></span></label>

                        <input type="radio" name='printTexture' id="print2" value='serebro.png'>
                        <label for="print2"><span data-currentprice="20" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/serebro.png)'></span></label>

                        <input type="radio" name='printTexture' id="print3" value="red.png">
                        <label for="print3"><span data-currentprice="30" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/red.png)'></span></label>

                        <input type="radio" name='printTexture' id="print4" value="raduga.png">
                        <label for="print4"><span data-currentprice="40" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/raduga.png)'></span></label>

                        <br>

                        <input type="radio" name='printTexture' id="print5" value="pink.png">
                        <label for="print5"><span data-currentprice="50" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/pink.png)'></span></label>

                        <input type="radio" name='printTexture' id="print6" value="perlamutr.png">
                        <label for="print6"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/perlamutr.png)'></span></label>

                        <input type="radio" name='printTexture' id="print7" value="malina.png">
                        <label for="print7"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/malina.png)'></span></label>

                        <input type="radio" name='printTexture' id="print8" value="gold-mat.png">
                        <label for="print8"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gold-mat.png)'></span></label>

                        <br>

                        <input type="radio" name='printTexture' id="print9" value="gold.png">
                        <label for="print9"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gold.png)'></span></label>

                        <input type="radio" name='printTexture' id="print10" value="black.png">
                        <label for="print10"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/black.png)'></span></label>

                        <input type="radio" name='printTexture' id="print11" value="gliter-violet.png">
                        <label for="print11"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gliter-violet.png)'></span></label>

                        <input type="radio" name='printTexture' id="print12" value="gliter-pink.png">
                        <label for="print12"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gliter-pink.png)'></span></label>
                    </div>
                </div>
                <!-- printTexture -->




                <!-- printTexture -->

            </div>
        </div>

    </div>


    <!-- -->



    <div id="hideFonts"></div> <!-- for init font in canvas -->
    <!-- /**/ -->
    <div class="printCarousel">
        <span class="icon-sliderNav leftSlide"></span>
        <span class="icon-sliderNav rightSlide"></span>
        <div class="carouselItems">
            <div class='item'>
                <a href="#" data-svgSrc='51wings.svg' data-left-position='0' data-top-position='10' data-width='100' >
                    <img src="<?= THEMROOT ?>/img/img/svg/51wings.svg" alt="" >
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='abracadabra.svg' data-left-position='5' data-top-position='20' data-width='90'>
                    <img src="<?= THEMROOT ?>/img/img/svg/abracadabra.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='AintLaurent.svg' data-left-position='5' data-top-position='20' data-width='90' >
                    <img src="<?= THEMROOT ?>/img/img/svg/AintLaurent.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='armian.svg' data-left-position='0' data-top-position='0' data-width='100'>
                    <img src="<?= THEMROOT ?>/img/img/svg/armian.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='bitch_better_have.svg' data-left-position='0' data-top-position='20' data-width='100' >
                    <img src="<?= THEMROOT ?>/img/img/svg/bitch_better_have.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='blessed.svg' data-left-position='5' data-top-position='20' data-width='90' >
                    <img src="<?= THEMROOT ?>/img/img/svg/blessed.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" class='fill' data-svgSrc='cake.svg' data-left-position='20' data-top-position='20' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/cake.svg" alt="">
                </a>
            </div>

            <div class="item">
                <img class='your-name' src="<?= THEMROOT ?>/img/basketprint.png" alt="" data-textTop="10" data-textSize="16" data-numberTop="20" data-numberSize="80" >
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='CHAMPAGNE.svg' data-left-position='5' data-top-position='20' data-width='90' >
                    <img src="<?= THEMROOT ?>/img/img/svg/CHAMPAGNE.svg" alt="">
                </a>
            </div>


            <div class='item'>
                <a href="#" data-svgSrc='champagne_is_always_the_answer_1.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/champagne_is_always_the_answer_1.svg" alt="">
                </a>
            </div>


            <div class='item'>
                <a href="#" data-svgSrc='chanelHM.svg' data-left-position='20' data-top-position='20' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/chanelHM.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='CocaineCaviar.svg' data-left-position='20' data-top-position='20' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/CocaineCaviar.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='darling_M_kak_kurazh.svg' data-left-position='0' data-top-position='15' data-width='100' >
                    <img src="<?= THEMROOT ?>/img/img/svg/darling_M_kak_kurazh.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='deerFas.svg' data-left-position='10' data-top-position='15' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/deerFas.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='deerProfile.svg' data-left-position='20' data-top-position='15' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/deerProfile.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='diamant_kapli.svg' data-left-position='25' data-top-position='25' data-width='50' >
                    <img src="<?= THEMROOT ?>/img/img/svg/diamant_kapli.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='dior_NW1.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/dior_NW1.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='dreaming.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/dreaming.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='fashion.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/fashion.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='fuck.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/fuck.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" class='fill'data-svgSrc='fuck_hand.svg' data-left-position='70' data-top-position='15' data-width='20' >
                    <img src="<?= THEMROOT ?>/img/img/svg/fuck_hand.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='georgian.svg' data-left-position='0' data-top-position='0' data-width='100' >
                    <img src="<?= THEMROOT ?>/img/img/svg/georgian.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='Giraunchu.svg' data-left-position='10' data-top-position='8' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/Giraunchu.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='GivenchyStars.svg' data-left-position='0' data-top-position='0' data-width='100' >
                    <img src="<?= THEMROOT ?>/img/img/svg/GivenchyStars.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='hebrew.svg' data-left-position='0' data-top-position='0' data-width='100' >
                    <img src="<?= THEMROOT ?>/img/img/svg/hebrew.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='holly_bitch.svg' data-left-position='20' data-top-position='20' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/holly_bitch.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" class='fill'data-svgSrc='iceCream.svg' data-left-position='70' data-top-position='15' data-width='20' >
                    <img src="<?= THEMROOT ?>/img/img/svg/iceCream.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='KEEP_CALM.svg' data-left-position='25' data-top-position='15' data-width='50' >
                    <img src="<?= THEMROOT ?>/img/img/svg/KEEP_CALM.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='kiss.svg' data-left-position='70' data-top-position='15' data-width='20' >
                    <img src="<?= THEMROOT ?>/img/img/svg/kiss.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='Krylya_na_vsu_spinu.svg' data-left-position='10' data-top-position='10' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/Krylya_na_vsu_spinu.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='Limited.svg' data-left-position='15' data-top-position='20' data-width='70' >
                    <img src="<?= THEMROOT ?>/img/img/svg/Limited.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='Louboutin.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/Louboutin.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='merde!.svg' data-left-position='10' data-top-position='15' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/merde!.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='MikkiHeart.svg' data-left-position='15' data-top-position='25' data-width='70' >
                    <img src="<?= THEMROOT ?>/img/img/svg/MikkiHeart.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='my_pussy.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/my_pussy.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='new_day_new_shit.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/new_day_new_shit.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='noParty.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/noParty.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='Pigeon.svg' data-left-position='5' data-top-position='0' data-width='90' >
                    <img src="<?= THEMROOT ?>/img/img/svg/Pigeon.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='porn.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/porn.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='prada.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/prada.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" class='fill' data-svgSrc='princess_disney.svg' data-left-position='10' data-top-position='25' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/princess_disney.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='queen.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/queen.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='rocket_banny.svg' data-left-position='20' data-top-position='15' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/rocket_banny.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='rockstar.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/rockstar.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='rose.svg' data-left-position='10' data-top-position='8' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/rose.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='sin.svg' data-left-position='5' data-top-position='25' data-width='90' >
                    <img src="<?= THEMROOT ?>/img/img/svg/sin.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='StayClassy.svg' data-left-position='10' data-top-position='25' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/StayClassy.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='svyataya.svg' data-left-position='20' data-top-position='25' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/svyataya.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='Swear.svg' data-left-position='15' data-top-position='15' data-width='70' >
                    <img src="<?= THEMROOT ?>/img/img/svg/Swear.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='The_Bitch_is_back.svg' data-left-position='15' data-top-position='20' data-width='70' >
                    <img src="<?= THEMROOT ?>/img/img/svg/The_Bitch_is_back.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='this_is_designer.svg' data-left-position='15' data-top-position='20' data-width='70' >
                    <img src="<?= THEMROOT ?>/img/img/svg/this_is_designer.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='transformer.svg' data-left-position='20' data-top-position='20' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/transformer.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" class='fill' data-svgSrc='twoFingers.svg' data-left-position='70' data-top-position='15' data-width='20' >
                    <img src="<?= THEMROOT ?>/img/img/svg/twoFingers.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='wash.svg' data-left-position='10' data-top-position='20' data-width='80' >
                    <img src="<?= THEMROOT ?>/img/img/svg/wash.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='wingsRounded.svg' data-left-position='0' data-top-position='25' data-width='1' >
                    <img src="<?= THEMROOT ?>/img/img/svg/wingsRounded.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='withCoco.svg' data-left-position='20' data-top-position='20' data-width='60' >
                    <img src="<?= THEMROOT ?>/img/img/svg/withCoco.svg" alt="">
                </a>
            </div>

            <div class='item'>
                <a href="#" data-svgSrc='youngnbeautiful.svg' data-left-position='0' data-top-position='0' data-width='100' >
                    <img src="<?= THEMROOT ?>/img/img/svg/youngnbeautiful.svg" alt="">
                </a>
            </div>
        </div>

    </div>



    <div class="inputPrintTextMob">
        <div class="wrapp_text">
            <input  class="text-string" maxlength="13" type="text" placeholder='Имя' >
            <span class='textStringError'>A-Z</span>
        </div>
        <div class="wrapp_number">
            <input  class="number-string" maxlength="2" type="text" placeholder='Номер'>
            <span class='numberStringError'>0-9</span>
        </div>
    </div>
    <div class="mobileControl">
        <ul class="tabsControl">
            <li>
                <a  class='mobType active' href="">тип</a>
            </li>
            <li>
                <a class='mobParam' href="">параметры</a>
            </li>
            <li>
                <a class='mobPrint' href="">принт</a>
            </li>
        </ul>
        <div class="clearfix"></div>

        <!-- firstMobTab -->
        <div class="firstMobTab active">
            <div class="leftSide">
                <div class="title">Тип</div>
                <div class="mobAligner">
                    <span class='valShirt'><i class='icon-sweatshirt'></i></span>
                    <ul class='listShirt'>
                        <li class='active' data-shirt='sweatshirt' data-name='Sweatshirt'  data-currentprice="20" data-baseprice="500"><span class='icon-sweatshirt'></span></li>
                        <li data-shirt='shirt'  data-name='Shirt'  data-currentprice="15" data-baseprice="300"><span class='icon-tshirt'></span></li>
                    </ul>
                </div>
            </div>
            <div class="rightSide">
                <div class="title">Пол</div>
                <div class="mobAligner">
                    <span class='valGender'><i class='icon-man'></i></span>
                    <div class="gender">
                        <input type="radio" name="genderM" data-gender='adult' id="adultMob">
                        <label for="adultMob"><span class='icon-man'></span></label>

                        <input type="radio" name="genderM" data-gender='child' id="childMob">
                        <label for="childMob"><span class='icon-child'></span></label>

                        <input type="radio" name="genderM" data-gender='adult' id="adultWMob">
                        <label for="adultWMob"><span class='icon-woman'></span></label>
                    </div>
                </div>
            </div>
        </div>
        <!-- firstMobTab -->


        <!-- secondMobTab -->
        <div class="secondMobTab">

            <div class="leftSide">
                <div class="title">Размер</div>
                <div class="tableSize"><a href="#">Таблица размеров</a></div>
                <div class="mobAligner">
                    <span class="valSize">s</span>
                    <span class="valSizeChild">123</span>
                    <div class="sizeAdult">
                        <ul>
                            <li class='active'><span>s</span></li>
                            <li><span>m</span></li>
                            <li><span>l</span></li>
                            <li><span>xl</span></li>
                            <li><span>xxl</span></li>
                        </ul>
                    </div>
                    <div class="sizeChild">
                        <ul>
                            <li class='active'><span>62</span></li>
                            <li><span>68</span></li>
                            <li><span>74</span></li>
                            <li><span>80</span></li>
                            <li><span>86</span></li>
                            <li><span>92</span></li>
                            <li><span>98</span></li>
                            <li><span>104</span></li>
                            <li><span>110</span></li>
                            <li><span>116</span></li>
                            <li><span>122</span></li>
                            <li><span>128</span></li>
                            <li><span>134</span></li>
                            <li><span>140</span></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="rightSide">
                <div class="title">цвет</div>
                <div class="mobAligner">
                    <span class="valColor"></span>
                    <div class="colorPalette ">
                        <div class="contentInner">
                            <input type="radio" id="mobColor1" name='tShirtTypeMob' data-color='grey' checked>
                            <label for="mobColor1"><span  style='box-shadow: rgba(155, 156, 154, 0.298039) 0px 0px 0px 10px;background: #9b9c9a' ></span></label>

                            <input type="radio" id="mobColor2" name='tShirtTypeMob' data-color='maroon'>
                            <label for="mobColor2"><span style='background: #5a050d'></span></label>

                            <input type="radio" id="mobColor3" name='tShirtTypeMob' data-color='mint'>
                            <label for="mobColor3"><span style='background: #00bfab'></span></label>

                            <input type="radio" id="mobColor4" name='tShirtTypeMob' data-color='pink'>
                            <label for="mobColor4"><span style='background: #fea4d1'></span></label>

                            <input type="radio" id="mobColor5" name='tShirtTypeMob' data-color='red'>
                            <label for="mobColor5"><span style='background: #ff0423'></span></label>

                            <input type="radio" id="mobColor6" name='tShirtTypeMob' data-color='black'>
                            <label for="mobColor6"><span style='background: #000000'></span></label>

                            <input type="radio" id="mobColor7" name='tShirtTypeMob' data-color='white'>
                            <label for="mobColor7"><span style='background: #ffffff'></span></label>

                            <input type="radio" id="mobColor8" name='tShirtTypeMob' data-color='crimson'>
                            <label for="mobColor8"><span style='background: #ff6aa0'></span></label>

                            <input type="radio" id="mobColor9" name='tShirtTypeMob' data-color='dgrey'>
                            <label for="mobColor9"><span style='background: #8c8e89'></span></label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- secondMobTab -->

        <div class="thirdMobTab">
            <div class="leftSide">
                <div class="title">Принт</div>
                <a href="#" class="removePrint" >Удалить принт</a>
                <div class="mobAligner">
                    <div class="printSlides">

                        <div class="contentInner">
                            <img src="<?= THEMROOT ?>/img/img/svg/51wings.svg" alt="">
                            <div class="textInputCanva">
                                <div class="wrapp_text">
                                    <input  class="text-string" maxlength="13" type="text" placeholder='Имя' >
                                    <span class='textStringError'>A-Z</span>
                                </div>
                                <div class="wrapp_number">
                                    <input  class="number-string" maxlength="2" type="text" placeholder='Номер'>
                                    <span class='numberStringError'>0-9</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="mobSliderPrint">
                    <span class="icon-sliderNav leftSlide"></span>
                    <span class="icon-sliderNav rightSlide"></span>
                    <div class="carouselItems">
                        <div class='item'>
                            <a href="#" data-svgSrc='51wings.svg' data-left-position='0' data-top-position='10' data-width='100' >
                                <img src="<?= THEMROOT ?>/img/img/svg/51wings.svg" alt="" >
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='abracadabra.svg' data-left-position='5' data-top-position='20' data-width='90'>
                                <img src="<?= THEMROOT ?>/img/img/svg/abracadabra.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='AintLaurent.svg' data-left-position='5' data-top-position='20' data-width='90' >
                                <img src="<?= THEMROOT ?>/img/img/svg/AintLaurent.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='armian.svg' data-left-position='0' data-top-position='0' data-width='100'>
                                <img src="<?= THEMROOT ?>/img/img/svg/armian.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='bitch_better_have.svg' data-left-position='0' data-top-position='20' data-width='100' >
                                <img src="<?= THEMROOT ?>/img/img/svg/bitch_better_have.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='blessed.svg' data-left-position='5' data-top-position='20' data-width='90' >
                                <img src="<?= THEMROOT ?>/img/img/svg/blessed.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" class='fill' data-svgSrc='cake.svg' data-left-position='20' data-top-position='20' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/cake.svg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <img class='your-name' src="<?= THEMROOT ?>/img/basketprint.png" alt="" data-textTop="10" data-textSize="16" data-numberTop="20" data-numberSize="80" >
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='CHAMPAGNE.svg' data-left-position='5' data-top-position='20' data-width='90' >
                                <img src="<?= THEMROOT ?>/img/img/svg/CHAMPAGNE.svg" alt="">
                            </a>
                        </div>


                        <div class='item'>
                            <a href="#" data-svgSrc='champagne_is_always_the_answer_1.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/champagne_is_always_the_answer_1.svg" alt="">
                            </a>
                        </div>


                        <div class='item'>
                            <a href="#" data-svgSrc='chanelHM.svg' data-left-position='20' data-top-position='20' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/chanelHM.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='CocaineCaviar.svg' data-left-position='20' data-top-position='20' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/CocaineCaviar.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='darling_M_kak_kurazh.svg' data-left-position='0' data-top-position='15' data-width='100' >
                                <img src="<?= THEMROOT ?>/img/img/svg/darling_M_kak_kurazh.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='deerFas.svg' data-left-position='10' data-top-position='15' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/deerFas.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='deerProfile.svg' data-left-position='20' data-top-position='15' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/deerProfile.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='diamant_kapli.svg' data-left-position='25' data-top-position='25' data-width='50' >
                                <img src="<?= THEMROOT ?>/img/img/svg/diamant_kapli.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='dior_NW1.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/dior_NW1.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='dreaming.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/dreaming.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='fashion.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/fashion.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='fuck.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/fuck.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" class='fill'data-svgSrc='fuck_hand.svg' data-left-position='70' data-top-position='15' data-width='20' >
                                <img src="<?= THEMROOT ?>/img/img/svg/fuck_hand.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='georgian.svg' data-left-position='0' data-top-position='0' data-width='100' >
                                <img src="<?= THEMROOT ?>/img/img/svg/georgian.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='Giraunchu.svg' data-left-position='10' data-top-position='8' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/Giraunchu.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='GivenchyStars.svg' data-left-position='0' data-top-position='0' data-width='100' >
                                <img src="<?= THEMROOT ?>/img/img/svg/GivenchyStars.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='hebrew.svg' data-left-position='0' data-top-position='0' data-width='100' >
                                <img src="<?= THEMROOT ?>/img/img/svg/hebrew.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='holly_bitch.svg' data-left-position='20' data-top-position='20' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/holly_bitch.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" class='fill'data-svgSrc='iceCream.svg' data-left-position='70' data-top-position='15' data-width='20' >
                                <img src="<?= THEMROOT ?>/img/img/svg/iceCream.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='KEEP_CALM.svg' data-left-position='25' data-top-position='15' data-width='50' >
                                <img src="<?= THEMROOT ?>/img/img/svg/KEEP_CALM.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='kiss.svg' data-left-position='70' data-top-position='15' data-width='20' >
                                <img src="<?= THEMROOT ?>/img/img/svg/kiss.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='Krylya_na_vsu_spinu.svg' data-left-position='10' data-top-position='10' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/Krylya_na_vsu_spinu.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='Limited.svg' data-left-position='15' data-top-position='20' data-width='70' >
                                <img src="<?= THEMROOT ?>/img/img/svg/Limited.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='Louboutin.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/Louboutin.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='merde!.svg' data-left-position='10' data-top-position='15' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/merde!.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='MikkiHeart.svg' data-left-position='15' data-top-position='25' data-width='70' >
                                <img src="<?= THEMROOT ?>/img/img/svg/MikkiHeart.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='my_pussy.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/my_pussy.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='new_day_new_shit.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/new_day_new_shit.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='noParty.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/noParty.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='Pigeon.svg' data-left-position='5' data-top-position='0' data-width='90' >
                                <img src="<?= THEMROOT ?>/img/img/svg/Pigeon.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='porn.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/porn.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='prada.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/prada.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" class='fill' data-svgSrc='princess_disney.svg' data-left-position='10' data-top-position='25' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/princess_disney.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='queen.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/queen.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='rocket_banny.svg' data-left-position='20' data-top-position='15' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/rocket_banny.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='rockstar.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/rockstar.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='rose.svg' data-left-position='10' data-top-position='8' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/rose.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='sin.svg' data-left-position='5' data-top-position='25' data-width='90' >
                                <img src="<?= THEMROOT ?>/img/img/svg/sin.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='StayClassy.svg' data-left-position='10' data-top-position='25' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/StayClassy.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='svyataya.svg' data-left-position='20' data-top-position='25' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/svyataya.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='Swear.svg' data-left-position='15' data-top-position='15' data-width='70' >
                                <img src="<?= THEMROOT ?>/img/img/svg/Swear.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='The_Bitch_is_back.svg' data-left-position='15' data-top-position='20' data-width='70' >
                                <img src="<?= THEMROOT ?>/img/img/svg/The_Bitch_is_back.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='this_is_designer.svg' data-left-position='15' data-top-position='20' data-width='70' >
                                <img src="<?= THEMROOT ?>/img/img/svg/this_is_designer.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='transformer.svg' data-left-position='20' data-top-position='20' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/transformer.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" class='fill' data-svgSrc='twoFingers.svg' data-left-position='70' data-top-position='15' data-width='20' >
                                <img src="<?= THEMROOT ?>/img/img/svg/twoFingers.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='wash.svg' data-left-position='10' data-top-position='20' data-width='80' >
                                <img src="<?= THEMROOT ?>/img/img/svg/wash.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='wingsRounded.svg' data-left-position='0' data-top-position='25' data-width='1' >
                                <img src="<?= THEMROOT ?>/img/img/svg/wingsRounded.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='withCoco.svg' data-left-position='20' data-top-position='20' data-width='60' >
                                <img src="<?= THEMROOT ?>/img/img/svg/withCoco.svg" alt="">
                            </a>
                        </div>

                        <div class='item'>
                            <a href="#" data-svgSrc='youngnbeautiful.svg' data-left-position='0' data-top-position='0' data-width='100' >
                                <img src="<?= THEMROOT ?>/img/img/svg/youngnbeautiful.svg" alt="">
                            </a>
                        </div>
                    </div>



                </div>
            </div>
            <div class="rightSide">
                <div class="title">Цвет Принта</div>
                <div class="mobAligner">
                    <span class="mobPrintCol" style='background-image: url(<?= THEMROOT ?>/img/img/printTexture/serebro-mat.png)'></span>
                    <div class="printTexture">
                        <div class="contentInner">
                            <input type="radio" name='printTextureMob' id="mobPrint1" value="serebro-mat.png">
                            <label for="mobPrint1"><span data-currentprice="10" data-baseprice="0" style='box-shadow: rgba(155, 156, 154, 0.298039) 0px 0px 0px 10px;background: url(<?= THEMROOT ?>/img/img/printTexture/serebro-mat.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint2" value='serebro.png'>
                            <label for="mobPrint2"><span data-currentprice="20" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/serebro.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint3" value="red.png">
                            <label for="mobPrint3"><span data-currentprice="30" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/red.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint4" value="raduga.png">
                            <label for="mobPrint4"><span data-currentprice="40" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/raduga.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint5" value="pink.png">
                            <label for="mobPrint5"><span data-currentprice="50" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/pink.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint6" value="perlamutr.png">
                            <label for="mobPrint6"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/perlamutr.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint7" value="malina.png">
                            <label for="mobPrint7"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/malina.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint8" value="gold-mat.png">
                            <label for="mobPrint8"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gold-mat.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint9" value="gold.png">
                            <label for="mobPrint9"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gold.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint10" value="black.png">
                            <label for="mobPrint10"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/black.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint11" value="gliter-violet.png">
                            <label for="mobPrint11"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gliter-violet.png)'></span></label>

                            <input type="radio" name='printTextureMob' id="mobPrint12" value="gliter-pink.png">
                            <label for="mobPrint12"><span data-currentprice="0" data-baseprice="0" style='background: url(<?= THEMROOT ?>/img/img/printTexture/gliter-pink.png)'></span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!--    --><?php //ACWProducts::addNewProduct('sd') ?>
<?php get_footer(); ?>
