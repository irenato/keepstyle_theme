<?php
/*
Template Name: checkout
*/
?>
<?php $products = ACWProducts::getAllProducts(); ?>
<?php if(!$products) header('Location: '.get_home_url()); ?>
<?php unset($_SESSION['acw_gift']); ?>
<?php unset($_SESSION['acw_delivery_price']); ?>

<?php $symbol = $_SESSION['acw_active_currency']; ?>

<?php get_header(); ?>

    <div class="basket">
        <div class="container">
            <div class="row title">
                <div class="col-lg-12">
                    корзина
                    <span class='blueLine'></span>
                </div>
            </div>
            <div class="row tabName">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="reg active">
                        <span>01. Оформдение Заказа</span>
                    </div>

                    <div class="lineHidden lineHidden-first visible-xs"></div>

                    <div class="delivery">
                        <span>02. Доставка</span>
                    </div>

                    <div class="lineHidden lineHidden-last visible-xs"></div>

                    <div class="payment">
                        <span> 03. Оплата</span>
                    </div>
                </div>
            </div>

            <div class="row  visible-xs"><div class="col-lg-12"><div class="blue_line"></div></div></div>

            <div class="regTab">
                <div class="row ">
                    <div class="titleBasket hidden-xs">
                        <div class="col-lg-3 col-md-3 col-sm-3">Product</div>
                        <div class="col-lg-3 col-md-3 col-sm-3">Print</div>
                        <div class="col-lg-2 col-md-2 col-sm-2">Price</div>
                        <div class="col-lg-2 col-md-2 col-sm-2">Quantity</div>
                        <div class="col-lg-2 col-md-2 col-sm-2">Total price</div>
                    </div>
                </div>


                <?php $total_price = 0; ?>
                <?php foreach($products as $key => $product): ?>
                    <?php $current_price = ACWCurrency::priceCalculation($product['price']); ?>
                    <?php $total_price += $current_price*$product['count']; ?>
                    <div class="row">
                        <div class="cellBacket">
                            <div class="titleProd visible-xs"><?= $product['name'] ?></div>
                            <span class="removeProduct" data-id="<?= $key ?>"><img src="<?= THEMROOT ?>/img/remove.png" alt=""></span>
                            <div class="col-lg-3 col-md-3 col-sm-3 product heightCell">
                                <span class="titleProd hidden-xs"><?= $product['name'] ?></span>
                                <a href="#"><img src="<?=  $product['image'] ?>" alt=""></a>
                                <div class="type">
                                    <?php if($product['status'] != 0):?>
                                        <span class='size'>Размер: <i><?= strtoupper($product['size']) ?></i></span>
                                        <span class="gender">Пол: <?= $product['sex'] ?></span>
                                    <?php endif; ?>
<!--                                    <span class="price visible-xs">Price: 120$</span>-->
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 print heightCell">
                                <?php if($product['status'] == 1): ?>
                                    <img src="<?= $product['printImg'] ?>" alt="">
                                    <span>Цвет: <i class="viewColor" style='background: url(<?= $product["print_color"] ?>);'></i></span>
                                <?php elseif($product['status'] == 2): ?>
                                    <?php $text = explode('|', $product['printText']) ?>
                                    <div class="textPrint">
                                        <div class='namePrint'><?= $text[0] ?></div>
                                        <div class='numPrint'><?= $text[1] ?></div>
                                    </div>
                                    <span>Цвет: <i class="viewColor" style='background: url(<?= $product["print_color"] ?>);'></i></span>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2  price heightCell">
                                <span><i class='visible-xs'>Price: </i><?= $current_price ?><?= $symbol['symbol'] ?></span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 quantity heightCell">
                                <span class="minVal" data-price="<?= $current_price ?>">-</span>
                                <span class='counter' data-id="<?= $key ?>"><?= $product['count'] ?></span>
                                <span class="maxVal" data-price="<?= $current_price ?>">+</span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 totPrice heightCell">
                                <span> <i class="visible-xs">Total price:</i><span class="product-total-price"><?= $current_price*$product['count'] ?></span><?= $symbol['symbol'] ?></span>
                            </div>
                        </div>

                        <div class="lineBasket "></div>
                    </div>
                <?php endforeach; ?>

                <div class="errorTitle cartErrorTitle">Oh, dear! Something went wrong!</div>

                <div class="row countPrice">
                    <div class="col-lg-6 col-md-7 col-sm-4 col-xs-12 leftSide">
                        <div class="certificate">
                            <input type="text" name="" id="" placeholder='Номер подарочной карты'>
                            <span class="botLine"></span>
                            <span class="rightLine"></span>
                        </div>
                        <input type="submit" class="enterCert" value='OK'>
                    </div>
                    <div class="col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-2 col-sm-4 col-sm-offset-4 col-xs-12 infoPay">
                        <p>Total products:<span><i id="product-total-price"><?= $total_price ?></i><?= $symbol['symbol'] ?></span></p>
                        <p>Gift card:<span><i id="gift-price">0</i><?= $symbol['symbol'] ?></span></p>
                        <span class="line"></span>
                        <div class="total">
                            Total: <span><i class="counter-total-price"><?= $total_price ?></i><?= $symbol['symbol'] ?></span>
                        </div>
                    </div>
                </div>

                <div class="row controllCheckout">
                    <div class="col-lg-12">
                        <input type="submit" value="Check out" id="next-tab">
                        <a href="<?= get_home_url() ?>">Continue shopping</a>
                    </div>
                </div>

            </div><!-- regTab -->




            <div class="row deliveryTab"> <!-- deliveryTab -->
                <div class="totalPrice"><!-- totalPrice -->
                    <div class="col-lg-12 ">
                        Total price: <i class="counter-total-price"><?= $total_price ?></i><?= $symbol['symbol'] ?>
                    </div>
                </div><!-- totalPrice -->
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <form class="main-information">
                        <div class="errorTitle mainErrorTitle">Oh, dear! Something went wrong!</div>
                        <div class="group">
                            <input type="email" name="email" class="current-value">
                            <span class="bot-line"></span>
                            <span class="right-line"></span>
                            <label>E-mail</label>
                        </div>

                        <div class="group">
                            <input type="text" name="last-name" class="current-value">
                            <span class="bot-line"></span>
                            <span class="right-line"></span>
                            <label>Фамилия</label>
                        </div>

                        <div class="group">
                            <input type="text" name="first-name" class="current-value">
                            <span class="bot-line"></span>
                            <span class="right-line"></span>
                            <label>Имя</label>
                        </div>

                        <div class="group">
                            <input type="text" name="phone" class="current-value">
                            <span class="bot-line"></span>
                            <span class="right-line"></span>
                            <label>Номер телефона</label>
                        </div>

                        <div class="group">
                            <input type="hidden" value="" name="country">
                            <div class="selectParent">
                                <span class='titleSelect'>Страна</span>
                                <div class="resultSelect countryResult" data-value=""></div>
                                <ul class="customeSelect countrySelect">
                                    <?php foreach(getCountryList() as $country): ?>
                                        <li data-value=""><?= $country->name ?></li>
                                    <?php endforeach; ?>
                                </ul>

                            </div>
                        </div>

                        <div class="group">

                            <input type="text" name="region" class="current-value">
                            <span class="bot-line"></span>
                            <span class="right-line"></span>

                            <label>Область</label>
                        </div>

                        <div class="group">

                            <input type="text" name="city" class="current-value">
                            <span class="bot-line"></span>
                            <span class="right-line"></span>

                            <label>Город</label>
                        </div>

                        <div class="group">
                            <input type="hidden" value="" name="delivery">
                            <div class="selectParent">
                                <span class='titleSelect deliveryTitle'>Способ доставки</span>
                                <div class="resultSelect deliveryResult" data-value=""></div>
                                <ul class="customeSelect deliverySelect">
                                </ul>
                            </div>
                        </div>

                        <div class="group delivery-details">

                            <input type="text" name="delivery-details" class="current-value">
                            <span class="bot-line"></span>
                            <span class="right-line"></span>

                            <label>Детали доставки</label>
                        </div>

                        <textarea placeholder="Примечание" name="comment"></textarea>

                        <input type="submit" value="Next">

                    </form>
                </div>
            </div><!-- deliveryTab -->

            <div class="row paymentTab">
                <div class="totalPrice"><!-- totalPrice -->
                    <div class="col-lg-12 ">
                        Total price: <i class="counter-total-price"><?= $total_price ?></i><?= $symbol['symbol'] ?>
                    </div>
                </div><!-- totalPrice -->
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div id="paymentForms" style="display: none;">
                    </div>
                    <form class="payment-form">

                        <div class="radioGroup">

                            <div class="secG">
                                <input type="radio" name="pay" id="receive" checked>
                                <label for="receive">Оплата при получении</label>
                            </div>
                            <div class="fstG">
                                <input type="radio" name="pay" id="bankCard">
                                <label for="bankCard">Оплата банковской карточкой</label>
                            </div>
                        </div>

                        <input class='payBtn' type="submit" value="PAY">
                        <ul class="payGroup">
                            <li><a href="#" id="liqPaySubmit">liqpay</a></li>
                            <li><a href="#" id="">webmoney</a></li>
                            <li><a href="#" id="payPalSubmit">paypal</a></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

<?php get_footer(); ?>