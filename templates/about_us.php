<?php
/*
Template Name: About Us
*/
?>

<?php get_header(); ?>
<?php $team = ACWTeam::getTeam(); ?>
<!-- aboutWrapper -->
<div class="aboutWrapper">
    <!-- our_team -->
    <div class="our_team">
        <div class="container">
            <div class="row">
                <div class="title">
                    наша команда
                    <span class="blueSmallLine"></span>
                </div>

            </div>
        </div>
        <?php if($team): ?>
            <div class="teamSlider">
                <?php $upload_dir = wp_upload_dir();?>
                <?php foreach($team as $object): ?>
                <div class="item">
                    <img src="<?= $upload_dir['baseurl'].'/'.$object->image1 ?>" data-mouseover="<?= $upload_dir['baseurl'].'/'.$object->image2 ?>" data-mouseout="<?= $upload_dir['baseurl'].'/'.$object->image1 ?>" alt="teamSlider">

                    <div class="rect">
                        <div class="sliderTitle">
                            <?php if(LANG_CODE == "ru"): ?>
                                <h5><?= $object->name_ru ?></h5>
                                <span><?= $object->position_ru ?></span>
                            <?php else: ?>
                                <h5><?= $object->name_en ?></h5>
                                <span><?= $object->position_en ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="line_1"></div>
                        <div class="line_2"></div>
                        <div class="line_3"></div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <!-- aboutUs -->
    <div class="aboutUs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?= the_post_thumbnail('full', array( 'class' => 'aboutBaner' )) ?>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 item">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- aboutUs -->

    <div class="aboutReviews">
        <div class="container">
            <div class="row">
                <div class="title">
                    отзывы
                    <span class="blueSmallLine"></span>
                </div>

            </div>
        </div>


        <div class="reviewInfo">
            <div class="container">
                <div class="row">
                    <?php getAllComments(); ?>
                    <a href="#" data-modal-open>Написать отзыв</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- aboutWrapper -->


<?php get_footer(); ?>
