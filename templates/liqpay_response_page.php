<?php
/*
Template Name: LiqPay CallBack
*/
?>

<?php

if(isset($_POST[data])){

    $decode_arr_data = base64_decode($_POST[data]);

    $json_decoder = json_decode($decode_arr_data);

    if($json_decoder->status == 'success' || $json_decoder->status == 'sandbox'){

        $data = ACWOrders::getDataFromFile($json_decoder->order_id);
        $collect_order = $data->collect_order;
        $user_info = $data->user_info;
        $products = $data->products;

        $payment_data = sprintf('Payment ID :  %s, Pay Type : %s, LiqPay Order ID : %s, Sender Card Bank : %s, Sender Card Country : %s, IP : %s, Amount : %s, Currency : %s', $json_decoder->payment_id, $json_decoder->paytype, $json_decoder->liqpay_order_id, $json_decoder->sender_card_bank, $json_decoder->sender_card_country, $json_decoder->ip, $json_decoder->amount, $json_decoder->currency);

        ACWOrders::createNewOrder($json_decoder->status, 'LiqPay', $collect_order, $user_info, $payment_data, $products);

    }
}

?>