<?php
/**
 * Created by PhpStorm.
 * User: Serg
 * Date: 22.04.2016
 * Time: 11:01
 */
?>

<?php if(is_front_page()): ?>
    <!DOCTYPE html>
    <html lang="<?= LANG_CODE ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
        <?php wp_head(); ?>
        <title><?= get_the_title() ?></title>
    </head>
    <?php require_once 'home_page.php'; ?>
<?php else: ?>
    <?php get_header(); ?>

    <div class="support">
        <div class="container">
            <div class="supportTitle"><?php if(is_page('faq')) echo "faq"; else echo "поддержка";  ?><span class="blueSmallLine"></span></div>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                     <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>


<?php endif; ?>


<?php get_footer(); ?>