<?php
/**
 * The main template file
 *
 * Learn more: {@link http://alscon-web.com}
 *
 * @package WordPress
 * @subpackage Keep Style
 * @since Keep Style 1.0
 */
?>
<?php if(is_front_page()): ?>
    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
        <?php wp_head(); ?>
        <title><?= get_the_title() ?></title>
    </head>
	<?php require_once 'home_page.php'; ?>
<?php else: ?>
    <?php get_header(); ?>

<?php endif; ?>


<?php get_footer(); ?>