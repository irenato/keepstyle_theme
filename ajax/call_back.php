<?php
/**
 * Created by PhpStorm.
 * User: Serg
 * Date: 22.04.2016
 * Time: 11:51
 */

add_action( 'wp_ajax_callBack', 'callBack' );
add_action( 'wp_ajax_nopriv_callBack', 'callBack' );

function callBack(){
    if($_POST['action'] == 'callBack'){
        $data = $_POST['data'];
        $email = get_option('admin_email');
        $to  = "<$email>" ;
        $subject = "KeepStyle - Перезвоните мне!";
        $message = '
                    <html>
                        <head>
                            <title>KeepStyle - Перезвоните мне!</title>
                        </head>
                        <body>
                            <h3>Сообщение от: '.$data[0]['value'].'</h3>
                            <h3>Номер телефона: '.$data[1]['value'].'</h3>
                        </body>
                    </html>';

        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: <keepstyle@info.com>\r\n";
        $headers .= "Bcc: keepstyle@info.com\r\n";

        mail($to, $subject, $message, $headers);
    }
    die();
}

add_action( 'wp_ajax_setActiveCurrency', 'setActiveCurrency' );
add_action( 'wp_ajax_nopriv_setActiveCurrency', 'setActiveCurrency' );

function setActiveCurrency(){
    if($_POST['action'] == 'setActiveCurrency'){
        $id = $_POST['id'];
        ACWCurrency::setCurrentCurrency($id);
    }

    die();
}


add_action( 'wp_ajax_addGiftCard', 'addGiftCard' );
add_action( 'wp_ajax_nopriv_addGiftCard', 'addGiftCard' );

function addGiftCard(){
    if($_POST['action'] == 'addGiftCard'){

        $data = [
            'name' => 'Gift Card',
            'image' => '/wp-content/themes/keepstyle/img/88.png',
            'size' => '',
            'sex' => '',
            'status' => 0,
            'printImg' => '',
            'print_color' => '',
            'printText' => $_POST['text'],
            'price' => $_POST['price'],
            'count' => 1
        ];

        ACWProducts::addNewProduct($data);

        echo '/'.LANG_CODE.'/checkout/';

    }

    die();
}

add_action( 'wp_ajax_addNewProduct', 'addNewProduct' );
add_action( 'wp_ajax_nopriv_addNewProduct', 'addNewProduct' );

function addNewProduct(){
    if($_POST['action'] == 'addNewProduct'){
        ACWProducts::addNewProduct($_POST['product']);
        echo '/'.LANG_CODE.'/checkout/';
    }

    die();
}

add_action( 'wp_ajax_changeProductsCount', 'changeProductsCount' );
add_action( 'wp_ajax_nopriv_changeProductsCount', 'changeProductsCount' );

function changeProductsCount(){
    if($_POST['action'] == 'changeProductsCount'){
        ACWProducts::changeCount($_POST['data']);
        $product_price = ACWProducts::returnTotalPrice();
        if($product_price != 0){
            $product_price = ACWCurrency::priceCalculation($product_price);

            if(isset($_SESSION['acw_gift'])){
                $gift_temp = $_SESSION['acw_gift'];
                $gift = ACWCurrency::priceCalculation($gift_temp['total']);
                if($product_price >= $gift)
                    $product_price -= $gift;
                else $product_price = 0;
            }

            echo $product_price;

        }else echo 'none';
    }

    die();
}

add_action( 'wp_ajax_deleteThisProduct', 'deleteThisProduct' );
add_action( 'wp_ajax_nopriv_deleteThisProduct', 'deleteThisProduct' );

function deleteThisProduct(){
    if($_POST['action'] == 'deleteThisProduct'){
//       if(ACWProducts::deleteProduct($_POST['id']) == 0) echo get_home_url();
        ACWProducts::deleteProduct($_POST['id']);
    }

    die();
}

add_action( 'wp_ajax_getGiftCertificate', 'getGiftCertificate' );
add_action( 'wp_ajax_nopriv_getGiftCertificate', 'getGiftCertificate' );

function getGiftCertificate(){
    if($_POST['action'] == 'getGiftCertificate'){
        $code = $_POST['code'];
        $total_price = $_POST['total'];
        global $wpdb;
        $table_name = $wpdb->prefix.'acw_gift';
        $sql = "SELECT `price` FROM $table_name WHERE `code` = '$code' AND `active` = 1;";
        $gift = $wpdb->get_var($sql);
        $_SESSION['acw_gift'] = ['code' => $code, 'total' => $gift];
        if($gift){

            $gift = ACWCurrency::priceCalculation($gift);

            if($gift <= $total_price){
                echo $gift;
                $gift = 0;
            }else{
                echo $total_price;
                $gift = $gift - $total_price;
            }

        }else echo 'none';

    }else echo 'none';

    die();
}


add_action( 'wp_ajax_getDeliveryMethod', 'getDeliveryMethod' );
add_action( 'wp_ajax_nopriv_getDeliveryMethod', 'getDeliveryMethod' );

function getDeliveryMethod(){
    if($_POST['action'] == 'getDeliveryMethod'){
        $country = $_POST['country'];
        $count = ACWProducts::returnTotalCount();
        $column_name = 'four';
        if($count <= 4)
            $column_name = 'four';
        elseif($count > 4 && $count <= 6)
            $column_name = 'six';
        elseif($count > 6 && $count <= 8)
            $column_name = 'eight';
        else $column_name = 'ten';

        if($country == 1){
            global $wpdb;
            $wpdb->flush();
            $table_name = $wpdb->prefix.'acw_pochta_rf';
            $city = $_POST['city'];
            $sql = "SELECT $column_name AS price FROM $table_name WHERE `status` = 0 AND (`city_ru` LIKE  '$city' OR `city_en` LIKE  '$city');";
            $price = $wpdb->get_var($sql);
            if($price){
                echo '<li data-value="'.$price.'">Почта России</li>';
            }else echo '<li data-value="0">Почта России</li>';

            $wpdb->flush();
            $sql = "SELECT $column_name AS price FROM $table_name WHERE `status` = 1 AND (`city_ru` LIKE  '$city' OR `city_en` LIKE  '$city');";
            $price = $wpdb->get_var($sql);
            if($price){
                echo '<li data-value="'.$price.'">Pony Express</li>';
            }else echo '<li data-value="0">Pony Express</li>';

        }elseif($country == 2){
            if(isset($_POST['city'])){
                global $wpdb;
                $wpdb->flush();
                $table_name = $wpdb->prefix.'acw_post';
                $city = mb_strtolower($_POST['city']);
                if($city == 'харьков' || $city == 'харків' || $city == 'kharkov' || $city == 'kharkiv')
                    $sql = "SELECT `name_ru`, `name_en`, `$column_name` AS price FROM $table_name WHERE `country_id` = 2 AND `city_id` = 1;";
                else $sql = "SELECT `name_ru`, `name_en`, `$column_name` AS price FROM $table_name WHERE `country_id` = 2 AND `city_id` = 0;";
                $post = $wpdb->get_results($sql);
                if($post){
                    foreach($post as $value)
                        echo '<li data-value="'.$value->price.'">'.$value->name_ru.'</li>';
                }
            }
        }else{

            global $wpdb;
            $wpdb->flush();
            $table_name = $wpdb->prefix.'acw_post';
            $sql = "SELECT `name_ru`, `name_en`, `$column_name` AS price FROM $table_name WHERE `country_id` = 0 AND `city_id` = 0;";
            $post = $wpdb->get_results($sql);
            if($post){
                foreach($post as $value)
                    echo '<li data-value="'.$value->price.'">'.$value->name_ru.'</li>';
            }
        }

    }

    die();
}

add_action( 'wp_ajax_generatePaymentForms', 'generatePaymentForms' );
add_action( 'wp_ajax_nopriv_generatePaymentForms', 'generatePaymentForms' );

function generatePaymentForms(){
    if($_POST['action'] == 'generatePaymentForms'){
        $main_info = $_POST['info'];
        $_SESSION['acw_user_info'] = $main_info;
        $delivery = $_POST['delivery'] == ''? 0 : $_POST['delivery'];
        $_SESSION['acw_delivery_price'] = $delivery;

        $product_price = ACWProducts::returnTotalPrice();
        if($product_price != 0){
            ACWOrders::collectOrderData();
            $product_price = ACWCurrency::priceCalculation($product_price);
            $delivery = ACWCurrency::priceCalculation($delivery);
            $product_price += $delivery;
            $code = 'UAH';
            if(isset($_SESSION['acw_active_currency'])){
                $temp = $_SESSION['acw_active_currency'];
                $code = $temp['code'];
            }

            if(isset($_SESSION['acw_gift'])){
                $gift_temp = $_SESSION['acw_gift'];
                $gift = ACWCurrency::priceCalculation($gift_temp['total']);
                if($product_price >= $gift)
                    $product_price -= $gift;
                else $product_price = 0;
            }

            $input = '<input type="hidden" value="'.$product_price.'" name="delivery-price">';
            $pp = ACWPaymentSystems::payPal($product_price, $code);
            $lp = ACWPaymentSystems::liqPay($product_price, $code);

            echo $input.$pp.$lp;
        }

    }

    die();
}

add_action( 'wp_ajax_createNewOrder', 'createNewOrder' );
add_action( 'wp_ajax_nopriv_createNewOrder', 'createNewOrder' );

function createNewOrder(){
    if($_POST['action'] == 'createNewOrder'){
        $status = $_POST['status'];
        $data = ACWOrders::getDataFromFile($_SESSION['acw_file_name']);
        $collect_order = $data->collect_order;
        $user_info = $data->user_info;
        $products = $data->products;

        ACWOrders::createNewOrder('Payment on delivery', '', $collect_order, $user_info, '', $products);

        echo '/'.LANG_CODE.'/successfull/';
    }

    die();
}

// Comment Functions
add_action( 'wp_ajax_createNewComment', 'createNewComment' );
add_action( 'wp_ajax_nopriv_createNewComment', 'createNewComment' );

function createNewComment(){
    if($_POST['action'] == 'createNewComment'){
        $res = $_POST['data'];
        $time = current_time('mysql');
        $data = array(
            'comment_post_ID' => $res['id'],
            'comment_author' => $res['name'],
            'comment_content' => $res['comment'],
            'comment_author_url'   => $res['img'],
            'comment_date' => $time,
            'comment_approved' => 0
        );

        wp_insert_comment($data);
    }

    die();
}

add_action( 'wp_ajax_actionUploadedFile', 'actionUploadedFile' );
add_action( 'wp_ajax_nopriv_actionUploadedFile', 'actionUploadedFile' );

function actionUploadedFile(){
    if($_POST['action'] == 'actionUploadedFile'){
        echo fileUploader('file', 2);
    }
    die();
}