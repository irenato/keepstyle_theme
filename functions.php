<?php
/**
 * Keep_Style functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Keep Style
 * @since Keep Style 1.0
 */
define('THEMROOT', get_stylesheet_directory_uri());
define('LANG_CODE', substr(get_locale(), 0, 2));

require_once "ajax/call_back.php";
require_once 'include/LiqPay.php';


add_theme_support( 'post-thumbnails' );

// Remove Admin bar
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar(){
    if (current_user_can('administrator') || !is_admin()) {
        show_admin_bar(false);
    }
}

// Register Styles/Scripts
function keep_style_enqueue_style(){
    wp_enqueue_style( 'myCss-grid', THEMROOT.'/css/grid.css', false );
    wp_enqueue_style( 'myCss-fonts', THEMROOT.'/css/fonts.css', false );
    wp_enqueue_style( 'myCss-style', THEMROOT.'/css/style.min.css', false );
    if(is_page(11) || is_page(13)){
        wp_enqueue_style( 'myCss-carousel', THEMROOT.'/css/owl.carousel.css', false );
    }

    if(is_page(2) || is_page(8)){
        wp_enqueue_style( 'myCss-slick', THEMROOT.'/css/slick.css', false );
        wp_enqueue_style( 'myCss-slick-theme', THEMROOT.'/css/slick-theme.css', false );
    }

}


function keep_style_enqueue_script(){
    wp_enqueue_script( 'myJs-jquery', THEMROOT . '/js/jquery-2.2.3.min.js', false, '', true);
    wp_enqueue_script( 'myJs-scroll', THEMROOT . '/js/jquery.nicescroll.min.js', false, '', true);

    if(is_front_page()){
        wp_enqueue_script( 'myJs-TweenMax', THEMROOT . '/js/TweenMax.min.js', false, '', true);
        wp_enqueue_script( 'myJs-masonry', THEMROOT . '/js/masonry.pkgd.min.js', false, '', true);
        wp_enqueue_script( 'myJs-home-page', THEMROOT . '/js/home-page.js', false, '', true);
    }

    if(is_page(11) || is_page(13)){
        wp_enqueue_script( 'myJs-fabric', THEMROOT . '/js/fabric.min.js', false, '', true);
        wp_enqueue_script( 'myJs-owl.carousel', THEMROOT . '/js/owl.carousel.min.js', false, '', true);
        wp_enqueue_script( 'myJs-constructor', THEMROOT . '/js/constructor.js', false, '', true);
        wp_enqueue_script( 'myJs-common', THEMROOT . '/js/common.js', false, '', true);
        wp_enqueue_script( 'myJs-tShirtCustomize', THEMROOT . '/js/tShirtCustomize.js', false, '', true);
    }

    wp_enqueue_script( 'myJs-common', THEMROOT . '/js/common.js', false, '', true);


    if(is_front_page()){
        wp_enqueue_script( 'myJs-parallax', THEMROOT . '/js/parallax.js', false, '', true);
    }

    if(is_page(2) || is_page(8)){
        wp_enqueue_script( 'myJs-slick', THEMROOT . '/js/slick.min.js', false, '', true);
        wp_enqueue_script( 'myJs-about-us', THEMROOT . '/js/about-us.js', false, '', true);
    }

    if(is_page(63) || is_page(65) || is_page(72) || is_page(74)){
        wp_enqueue_script( 'myJs-select', THEMROOT . '/js/select.js', false, '', true);
    }

    wp_enqueue_script( 'myJs-main', THEMROOT . '/js/main.js', false, '', true);
    wp_localize_script( 'myJs-main', 'sendAjax',
        array('url'   => admin_url('admin-ajax.php'))
    );
}

add_action( 'wp_enqueue_scripts', 'keep_style_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'keep_style_enqueue_script' );

// Language widget
function keep_style_language_switcher() {
    $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
    if ( !empty( $languages ) ) {

        if(!isset($_SESSION['acw_active_currency']))
            ACWCurrency::setCurrentCurrency();
        $active_curr = $_SESSION['acw_active_currency'];

        foreach( $languages as $lang ) {
            if($lang['active'])
                echo '<span class="currencyName"><img src="'.$lang['country_flag_url'].'"><b>'.$active_curr['code'].'</b><i class="icon-arrow"></i></span>';
        }

        $lang = null;
        echo '<div class="choose-lang" data-state="close"><div class="titleLang">Language</div><ul class="flags">';
        foreach( $languages as $lang ) {
                echo '<li><a href="'.$lang['url'].'"><img src="'.$lang['country_flag_url'].'">'.$lang['native_name'].'</a></a></li>';
        }

        $currency = $_SESSION['acw_currency'];
        echo '</ul><div class="titleLang">Currency</div><ul class="currencyList">';
        $cnt = 0;
        if($currency)
            foreach($currency as $val){
                echo '<li><a href="#" data-id="'.$cnt.'">'.$val['code'].'</a></li>';
                $cnt++;
            }
        echo '</ul></div>';
    }

}


// Main menu
//register_nav_menu('menu','Main menu');
//register_nav_menu('menuMobile','Mobile menu');
register_nav_menus( array(
    'menu' => 'Main menu',
    'menuMobile' => 'Mobile menu',
) );
// Get country
function getCountryList(){
    global $wpdb;
    $table_name = $wpdb->prefix.'acw_country';
    $country_column = 'country_name_'.LANG_CODE;
    $sql = "SELECT $country_column AS name FROM $table_name ORDER BY $country_column";
    return $wpdb->get_results($sql);
}

function fileUploader($uploaded_file, $post_id){

    $attachment_id = media_handle_upload( $uploaded_file, $post_id);
    $attachment_url = wp_get_attachment_url($attachment_id);

    if ($attachment_url ) {
        return $attachment_url;
    } else {
        return "";
    }
}

function getAllComments(){
    $args = array(
        'post_id' => get_the_ID(),
        'status' => 'approve'
    );
    $comments = get_comments($args);
    if($comments){
        $count = 0;
        foreach($comments as $comment){
            echo '<div class="col-lg-6 col-md-6 col-sm-12 item">';
            echo '<div class="itemImage"><img style="max-height: 180px" src="'.$comment->comment_author_url.'" alt=""><i>'.$comment->comment_author.'</i></div>';
            echo '<div class="itemContent"><span>'.mysql2date('d.m.Y', $comment->comment_date).'</span>'.$comment->comment_content.'</div>';
            echo '</div><div class="clearfix visible-sm visible-xs"></div>';
            $count++;
            if($count == 2) {
                echo '</div><div class="clearfix visible-lg visible-md"></div>';
                $count = 0;
            }
        }

    }
}

