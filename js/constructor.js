/*
Created by Vladimir Razin;
E-mail: wowarazin@yandex.ru
*/


var canvas;

$(document).ready(function() {
	var line1, line2, line3, line4;
	var sel = 'black.png'; // standart texture
	var frontTexture;
	var backTexture;
	var leftSvgPos;
	var topSvgPos;
	// var left;
	// var top;
	var visibleText;	
	var printImage;
	var pattern;
	var path 			= '/wp-content/themes/keepstyle/img/img/';
	var leftControl 	= $('#left-control'); 
	var topControl 		= $('#top-control');
	var type 			= 'front';
	var printInCanvas 	= false;
	var textInCanvas 	= false;
	var printWithFill   = false;
	var srcL;
	var ww = $('#FrontCanvas').innerWidth();
	var hh = $('#FrontCanvas').innerHeight();
	var age = 'adult';
	var priceShirt 	 = 0,
		priceTexture = 0;
		priceShirt = $('.listShirt .active').data('currentprice');
	var	nameShirt = $('.listShirt .active').data('name');
	$('.printInformation .price i').text(priceShirt);
	$('.printInformation .title ').text(nameShirt);


	var options = {
		front: {
			visibleText: false,
			textInput: new fabric.Text('', {
				originX: 'left',
				originY: 'top',
				fontSize: parseInt($('.your-name').attr('data-textSize')*ww/100),
				fontFamily: 'MotorCyr',
				angle: 0,
				fontWeight: 'bold',	
				hasControls: false,
				hasBorders: false,
				top: parseInt($('.your-name').attr('data-textTop')*hh/100),
				id: 'textFront',
				selectable: false
			}),
			numberInput: new fabric.Text('', {
				originX: 'left',
				originY: 'top',
				top: parseInt($('.your-name').attr('data-numberTop')*hh/100),
				fontFamily: 'MotorCyr',
				angle: 0,
				fontWeight: 'bold',
				fontSize: parseInt($('.your-name').attr('data-numberSize')*ww/100),		
				hasControls: false,
				hasBorders: false,
				selectable: false
			})
		},
		back: {
			visibleText: false,
			textInput: new fabric.Text('', {
				originX: 'left',
				originY: 'top',
				fontSize: 52,
				fontFamily: 'MotorCyr',
				angle: 0,
				fontWeight: 'bold',		
				hasControls: false,
				hasBorders: false,
				top: 50,
				id: 'textBack'
			}),
			numberInput : new fabric.Text('', {
				originX: 'left',
				originY: 'top',
				top: 100,
				fontFamily: 'MotorCyr',
				angle: 0,
				fontWeight: 'bold',
				fontSize: 300,		
				hasControls: false,
				hasBorders: false
			})
		}
	};

	var inSvg = {
		front: {
			visibleSVG: false,
			svg: function(srcSvg, leftSvgPos, topSvgPos, widthSvg) {
				
				fabric.loadSVGFromURL(srcSvg, function(objects, options) {
					var obj = new fabric.util.groupSVGElements(objects, options);
					obj.set({
						left: leftSvgPos,
						top:  topSvgPos,
						hasControls: false,
						hasBorders: false

					}).scaleToWidth(widthSvg).setCoords();
					canvas.add(obj).renderAll();
					canvas.getActiveObject();
					canvas.setActiveObject(obj);
					patternSvg(path + 'printTexture/' + sel, svgToGroup);
				});
			}
		},
		back: {
			visibleSVG: false,
			svg: function(srcSvg, leftSvgPos, topSvgPos, widthSvg) {
				
				fabric.loadSVGFromURL(srcSvg, function(objects, options) {
					var obj = new fabric.util.groupSVGElements(objects, options);
					obj.set({
						left: leftSvgPos,
						top:  topSvgPos,
						hasControls: false,
						hasBorders: false,
						selectable: false

					}).scaleToWidth(widthSvg).setCoords();
					canvas.add(obj).renderAll();
					canvas.getActiveObject();
					canvas.setActiveObject(obj);

					patternSvg(path + 'printTexture/' + sel, svgToGroup);
				});
			}
		}
	}

	var inSvgWithFill = {
		front: {
			visibleSVG: false,
			svg: function(srcSvg, leftSvgPos, topSvgPos, widthSvg) {
				
				fabric.loadSVGFromURL(srcSvg, function(objects, options) {
					var obj = new fabric.util.groupSVGElements(objects, options);
					obj.set({
						left: leftSvgPos,
						top:  topSvgPos,
						hasControls: false,
						hasBorders: false,
						selectable: false

					}).scaleToWidth(widthSvg).setCoords();
					canvas.add(obj).renderAll();
					canvas.getActiveObject();
					canvas.setActiveObject(obj);
					patternSvg(path + 'printTexture/' + sel);
				});
			}
		}
	}

	var allProduct = {
		child : {
			sweatshirt : {
				black: {
					frontSrc: 'child_front_black.png',
					backSrc: 'child_back_black.png'
				},
				crimson: {
					frontSrc: 'child_front_crimson.png',
					backSrc: 'child_back_crimson.png'
				},
				dgrey: {
					frontSrc: 'child_front_dgrey.png',
					backSrc: 'child_back_dgrey.png'
				},
				grey: {
					frontSrc: 'child_front_grey.png',
					backSrc: 'child_back_grey.png'
				},
				maroon: {
					frontSrc: 'child_front_maroon.png',
					backSrc: 'child_back_maroon.png'
				},
				mint: {
					frontSrc: 'child_front_mint.png',
					backSrc: 'child_back_mint.png'
				},
				pink: {
					frontSrc: 'child_front_pink.png',
					backSrc: 'child_back_pink.png'
				},
				white : {
					frontSrc: 'child_front_white.png',
					backSrc: 'child_back_white.png'
				}
			},
			shirt: {
				black: {
					frontSrc: 'child_shirt_front_black.png',
					backSrc: 'child_shirt_back_black.png'
				},
				grey: {
					frontSrc: 'child_shirt_front_grey.png',
					backSrc: 'child_shirt_back_grey.png'
				},
				pink: {
					frontSrc: 'child_shirt_front_pink.png',
					backSrc: 'child_shirt_back_pink.png'
				},
				white: {
					frontSrc: 'child_shirt_front_white.png',
					backSrc: 'child_shirt_back_white.png'
				},
				red: {
					frontSrc: 'child_shirt_front_red.png',
					backSrc: 'child_shirt_back_red.png'
				}
			}
		},
		adult: {
			sweatshirt : {
				black: {
					frontSrc: 'sweatshirt_front_black.png',
					backSrc: 'sweatshirt_back_black.png'
				},
				crimson: {
					frontSrc: 'sweatshirt_front_crimson.png',
					backSrc: 'sweatshirt_back_crimson.png'
				},
				dgrey: {
					frontSrc: 'sweatshirt_front_dgrey.png',
					backSrc: 'sweatshirt_back_dgrey.png'
				},
				grey: {
					frontSrc: 'sweatshirt_front_grey.png',
					backSrc: 'sweatshirt_back_grey.png'
				},
				maroon: {
					frontSrc: 'sweatshirt_front_maroon.png',
					backSrc: 'sweatshirt_back_maroon.png'
				},
				mint: {
					frontSrc: 'sweatshirt_front_mint.png',
					backSrc: 'sweatshirt_back_mint.png'
				},
				pink: {
					frontSrc: 'sweatshirt_front_pink.png',
					backSrc: 'sweatshirt_back_pink.png'
				},
				red: {
					frontSrc: 'sweatshirt_front_red.png',
					backSrc: 'sweatshirt_back_red.png'
				},
				white : {
					frontSrc: 'sweatshirt_front_white.png',
					backSrc: 'sweatshirt_back_white.png'
				}
			},
			shirt: {
				black: {
					frontSrc: 'shirt_front_black.png',
					backSrc: 'shirt_back_black.png'
				},
				grey: {
					frontSrc: 'shirt_front_grey.png',
					backSrc: 'shirt_back_grey.png'
				},
				pink: {
					frontSrc: 'shirt_front_pink.png',
					backSrc: 'shirt_back_pink.png'
				},
				white: {
					frontSrc: 'shirt_front_white.png',
					backSrc: 'shirt_back_white.png'
				},
				red: {
					frontSrc: 'shirt_front_red.png',
					backSrc: 'shirt_back_red.png'
				}
			}
		}

	}

	
	canvas = this.__canvas = new fabric.Canvas('FrontCanvas', {
		selection: false,
		selectable: false
	});
	fabric.Object.prototype.transparentCorners = false;

	canvas.on({
		'object:moving': function(e) {	
			var ww = $('#FrontCanvas').innerWidth();
			var hh = $('#FrontCanvas').innerHeight();

			e.target.opacity = 0.5;
			var targetLeft = e.target.left*100/ww;
			var targetTop  = e.target.top*100/hh; 

			$('#left-control').val(parseInt(targetLeft, 10));
			$('#top-control').val(parseInt(targetTop, 10));

		},
		'object:modified': function(e) {		  	
			e.target.opacity = 1;
			canvas.getActiveObject();
		},
		'mouse:down': function(e) {
			canvas.setActiveObject(canvas.item(0));
		}
	});

	leftControl.on('keyup', function() {
		var item = canvas.getActiveObject();

		if (item) {

			ww = $('#FrontCanvas').innerWidth();

			item.setLeft(parseInt(this.value*ww/100, 10)).setCoords();
			canvas.renderAll();
		}
	});

	topControl.on('keyup', function() {
		var item = canvas.getActiveObject();
		if (item) {

			hh = $('#FrontCanvas').innerHeight();

			item.setTop(parseInt(this.value*hh/100, 10)).setCoords();
			canvas.renderAll();
		}
	});

	$('#widthPrint').keyup(function(){
		var val = $(this).val();
		clearCanvas();

		ww = $('#FrontCanvas').innerWidth();
		widthSvg 	= parseInt($(this).val()*ww/100);
		inSvgWithFill[type].svg(srcL, leftSvgPos, topSvgPos, widthSvg);

	});   

	/* func printTexture */
	function printTexture(url) {
		fabric.util.loadImage(url, function(img) {
			var type = 'front';

			options[type].textInput.fill = new fabric.Pattern({
				source: img,
				offsetX: 0,
				offsetY: 0
			});

			options[type].numberInput.fill = new fabric.Pattern({
				source: img,
				offsetX: 30,
				offsetY: 0
			});

			canvas.renderAll();
		});
	}
	/* func printTexture */

	// if(data-type="front")
	if(options[type].visibleText == true) {
		printTexture('../img/printTexture/gliter-violet.png');
	}

	$('.your-name').on('click', function() {

		if($(window).width()> 580) {
			$('.textInputCanva').css({
				display: 'inline-block'
			});
		} else {
			$('.inputPrintTextMob').show()
		}
		
		$('.printSlides img').attr('src', $(this).attr('src'));
		var type 		= 'front';
		textInCanvas 	= true;
		printInCanvas 	= false;


		canvas.remove(options[type].textInput);
		options[type].textInput.text 	= 'Your name';
		options[type].numberInput.text 	= '00';

		options[type].visibleText = true;
		clearCanvas();
		SetYourName(true);
		canvas.remove(printImage); 	
		$('.printTexture, .setColorPrint').show();
		printTexture(path + 'printTexture/' + sel );
		canvas
		.add(options[type].textInput, options[type].numberInput)
		// .setActiveObject(options[type].textInput)
		.centerObjectH(options[type].textInput)
		.centerObjectH(options[type].numberInput)
		.renderAll();

		options[type].visibleText = true;
		inSvg[type].visibleSVG	  = false;
		// $('#position').show();
		// $(".removePrint").show();	

	});

	/*#text-string*/
	$('.text-string').on('focus', function() {
		var type = 'front';
		canvas.setActiveObject(options[type].textInput);		
		options[type].visibleText 	=  true;
		inSvg[type].visibleSVG 		=  false;
		options[type].textInput.text 	= '';
	});

	$('.text-string').on('keyup', function(e) {
		var values = $(this).val();
		var re = /^[a-zA-Z\s\b]*$/ig;
		var m = re.exec(values);
		$('textStringError').hide();
		if(!m) {
			$('.textStringError').show();
			return false;
		} else if  ($(values).length<1) {
			$('.textStringError').hide();
		}

		valLength =  $(values).length;

		$(this).css({
			letterSpacing: valLength + 'px'
		})

		var type = 'front';
		canvas.setActiveObject(options[type].textInput);

		var active = canvas.getActiveObject();		
		options[type].textInput.text = $(this).val();		

		canvas
		.remove(active)
		.renderAll()
		.add(options[type].textInput)
		.centerObjectH(options[type].textInput);

		options[type].visibleText 	=  true;
		inSvg[type].visibleSVG		=  false;
		// $('#position').show();
		// $(".removePrint").show();



	});
	/*#text-string*/

	$('.number-string').on('focus', function() {
		var type = 'front';
		canvas.setActiveObject(options[type].numberInput);
		options[type].visibleText 	=  true;
		inSvg[type].visibleSVG 		=  false;
		options[type].numberInput.text 	= '';
	});	

	$('.number-string').on('keyup', function(e) {
		var values = $(this).val();
		var re = /^[0-9]*$/ig;
		var m = re.exec(values);
		$('.numberStringError').hide();
		if(!m) {
			$('.numberStringError').show();
			return false;
		}else if  ($(values).length<1) {
			$('.numberStringError').hide();
		}
		var type = 'front';
		canvas.setActiveObject(options[type].numberInput);

		var active = canvas.getActiveObject();	
		options[type].numberInput.text = $(this).val();	

		canvas
		.remove(active)
		.renderAll()
		.add(options[type].numberInput)
		.centerObjectH(options[type].numberInput);

		options[type].visibleText = true;
		inSvg[type].visibleSVG =  false;
		// $('#position').show();
		// $(".removePrint").show();	

	});

	addHideFont();

	function addHideFont() { 
		/*for render font*/
		HideFont = new fabric.Text('Hide text', {
			fontSize: 0,
			fontFamily: 'MotorCyr',
			fill: '#000000',	
			hasControls: false,
			hasBorders: false
		});

		canvas.renderAll().add(HideFont);
	}



	$('.removePrint').on({
		click: function(event) {
			event.preventDefault();
			var type = 'front';
			clearCanvas();
			inSvg[type].visibleSVG =  false;
			inSvg[type].visibleSVG =  false;
			options.front.visibleText = false;
			options.back.visibleText = false;
			SetYourName(false);

		}
	});

	$(".printArea").hover(
		function() { 	        	
			canvas.add(line1);
			canvas.add(line2);
			canvas.add(line3);
			canvas.add(line4); 
			canvas.renderAll();
		},
		function() {	        	
			canvas.remove(line1);
			canvas.remove(line2);
			canvas.remove(line3);
			canvas.remove(line4);
			canvas.renderAll();
		}
		);


	$(".flip").click(function(event) {
	// canvas.clear();
	// SetYourName(false);
	event.preventDefault();
	if ($(this).data('type') == 'front') {
		/*зад*/
		var backSRC = $('#mainCanvas img').attr('data-back');


			$("#mainCanvas img").attr("src", backSRC);
			// $(this).text('Показать вид спереди').data('type', 'back');
			$(this).find('span').text('Front view'); // change 10.05.2016
			$(this).data('type', 'back');	
			$(this).attr('data-type', 'back');	

		} else {
			/*перед*/
			var frontSRC = $('#mainCanvas img').attr('data-front');

			$("#mainCanvas img").attr("src", frontSRC);
			$(this).find('span').text('Back view');
			$(this).data('type', 'front');
			$(this).attr('data-type', 'front');	
			
		}
	})
	var totalPr = $('.printInformation .price i').text();
	totalPr  = parseInt(totalPr);

	function listShirtClick(controllClass, thisClass) {
		$(controllClass + '.gender input').each(function(index, el) {
			if($(this).prop('checked')) {
				age =  $(this).data('gender');
			}
		});
		var color 		= $(controllClass + ' .colorPalette input:checked').data('color');
		var typeProduct = thisClass.data('shirt');
		nameShirt 		= thisClass.data('name');

		priceShirt = thisClass.data('currentprice');
		priceShirt= parseInt(priceShirt);

		//var price;
		$(controllClass + ' .printTexture [type="radio"]').each(function(index, el) {
			if($(this).prop('checked')) {
				var thisSpan = $(this).next('label').find('span');
				priceTexture = thisSpan.data('currentprice');

			}
		});
		priceTexture = parseInt(priceTexture);
		var count = $('.printInformation .counter').text();
		count = parseInt(count);

		totalPr = (priceTexture+priceShirt)*count;

		$('.printInformation .title').text(nameShirt);

		$('.printInformation .price i').text(totalPr);

		$(controllClass + ' .listShirt li').removeClass('active');
		thisClass.addClass('active');
		if ($('.flip').data('type') == 'front') {
			loadTShirt( path + allProduct[age][typeProduct][color].frontSrc, path + allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].backSrc);
		} else {
			loadTShirt( path + allProduct[age][typeProduct][color].backSrc, path + allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].backSrc);
		}
	}

	$('.left-controll .listShirt li').on('click', function() {
		listShirtClick('.left-controll', $(this))
	});
	$('.mobileControl .listShirt li').on('click', function() {
		listShirtClick('.mobileControl', $(this))
	});

$('.printInformation .maxVal').click(function(event) {
	var inc   = $('.printInformation .price i').text();
	inc       = parseInt(inc);
	var count = $('.printInformation .counter').text();
	count = parseInt(count);
	var one = inc/count;
	inc = inc + one;

	$('.printInformation .price i').text(inc);
});

$('.printInformation .minVal').click(function(event) {
	var inc   = $('.printInformation .price i').text();
	inc       = parseInt(inc);
	var count = $('.printInformation .counter').text();
	count = parseInt(count);
	var one = inc/count;
	inc = inc - one;
	if(inc <= 0) {
		inc = one;
	}
	$('.printInformation .price i').text(inc);
});

$('.printTexture [type="radio"]').on('change', function() {
	if( $(this).prop('checked') ) {
		var thisSpan = $(this).next('label').find('span');
		priceTexture = thisSpan.data('currentprice');
		priceTexture = parseInt(priceTexture);
		priceShirt = $('.listShirt .active').data('currentprice');
		priceShirt = parseInt(priceShirt);
		var count = $('.printInformation .counter').text();
		count = parseInt(count);
		totalPr = (priceTexture + priceShirt)*count;
		$('.printInformation .price i ').text(totalPr);
	}
})


$('.left-controll .colorPalette input').on('change', function() {
	if( $(this).prop('checked') ) {
		colorPaletteChange('.left-controll', $(this));
	}
});

	$('.mobileControl .colorPalette input').on('change', function() {
		if( $(this).prop('checked') ) {
			colorPaletteChange('.mobileControl', $(this));
		}
	});

	function colorPaletteChange(controllClass, thisClass) {
		var color 		= thisClass.data('color');

		var typeProduct = $(controllClass + ' .listShirt li.active').data('shirt');
		$(controllClass + ' .gender input').each(function(index, el) {
			if($(this).prop('checked')) {
				age =  $(this).data('gender');
			}
		});

		if ($('.flip').data('type') == 'front') {
			// if(allProduct[age][typeProduct][color] != undefined) {
			loadTShirt( path + allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].backSrc);
			// }
		} else {
			loadTShirt( path + allProduct[age][typeProduct][color].backSrc, path+ allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].backSrc);
		}
	}

$('.left-controll .gender input').on('change', function() {
	if( $(this).prop('checked') ) {
		genderInputChange('.left-controll', $(this));
	}
});

$('.mobileControl .gender input').on('change', function() {
	if( $(this).prop('checked') ) {
		genderInputChange('.mobileControl', $(this));
	}
});

	function genderInputChange(controllClass, thisClass) {

		var color 		= $(controllClass + ' .colorPalette input:checked').data('color');
		var typeProduct = $(controllClass + ' .listShirt li.active').data('shirt');
		//console.log(color);
		//console.log(typeProduct);
		age 		= thisClass.data('gender');
		//console.log(age);
		if($(window).width()>=1200) {
			if(age === 'adult') {
				$('.printInformation .sizeAdult').show();
				$('.printInformation .sizeChild').hide();
			} else if (age === 'child') {
				$('.printInformation .sizeAdult').hide();
				$('.printInformation .sizeChild').show();
			}
		}

		if ($('.flip').data('type') == 'front') {
			loadTShirt( path + allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].backSrc);
		} else {
			loadTShirt( path + allProduct[age][typeProduct][color].backSrc, path+ allProduct[age][typeProduct][color].frontSrc, path+ allProduct[age][typeProduct][color].backSrc);
		}
	}

function loadTShirt(url, front, back) {
	fabric.util.loadImage(url, function(img) {
		canvas.renderAll();
		source = url;
		var Img = $('#mainCanvas img');
		Img.attr('src', url);
		Img.attr('data-front', front);
		Img.attr('data-back', back);
	});
}



// $('.svgTxt input').on('change', function() {
// 	sel  = this.value;	
// 	patternSvg(path + 'printTexture/' + sel , svgToGroup);
// });


$('.left-controll .printTexture input').on('change', function() {
	if( $(this).prop('checked') ) {
		printTextureChange(this);
		//console.log(this)
	}		
});
$('.mobileControl .printTexture input').on('change', function() {
	if( $(this).prop('checked') ) {
		printTextureChange(this);
		//console.log(this)
	}
});

	function printTextureChange(thisClass){
		sel  = thisClass.value;
		if (textInCanvas && !printInCanvas)  {
			printTexture(path + 'printTexture/' + sel );
		} else if(!textInCanvas && printInCanvas && !printWithFill) {
			patternSvg(path + 'printTexture/' + sel , svgToGroup);
		} else {
			return false;
		}
	}

function patternSvg(url, mask) {
	fabric.util.loadImage(url, function(img) {

		pattern = new fabric.Pattern({
			source: img,
			offsetX: 0,
			offsetY: 0
		});
		if(mask) {
			mask();
		}
		return;
		canvas.renderAll();
	});

}
// ldSVG('../../build/img/printTexture/gliter-pink.png');
// var pattern = new fabric.Pattern({
// 	source: '../../build/img/printTexture/gliter-pink.png' 
// });

if( inSvg[type].visibleSVG === undefined) {
	printTexture(path + 'printTexture/gliter-violet.png', svgToGroup);
}

function svgToGroup () {
	var obj = canvas.getActiveObject();
	if (!obj) return;

	if (obj.fill instanceof fabric.Pattern) {
		obj.fill = null;
	}
	else {
		if ( obj instanceof fabric.PathGroup ) {
			obj.getObjects().forEach(function(o) { o.fill = pattern;obj.selectable = false; });
		}
		else {
			obj.fill = pattern;			
		}
	}
	canvas.renderAll();
}

// var srcL = 'http://fabricjs.com/assets/100.svg';
// var path = '../build/';


/*create svg print*/
$('.printCarousel a:not(.fill),.mobSliderPrint a:not(.fill)').click(function (event) {
	// alert('no-fill');
	$('.textInputCanva').hide();
	$('.inputPrintTextMob').hide();
	$('.printSlides img').attr('src', $(this).find('img').attr('src'));
	event.preventDefault();
	clearCanvas();
	var type = 'front';
	printInCanvas 	= true;
	textInCanvas 	= false;
	printWithFill 	= false;
	// options[type].visibleText 	= false;
	// inSvg[type].visibleSVG 		= true;
	// 
	leftSvgPos 	= $(this).attr('data-left-position');
	topSvgPos 	= $(this).attr('data-top-position');


	widthSvg  	= $(this).attr('data-width');
	ww = $('#FrontCanvas').innerWidth();
	hh = $('#FrontCanvas').innerHeight();
	
	leftSvgPos 	= parseInt(leftSvgPos*ww/100);
	widthSvg 	= parseInt(widthSvg*ww/100);
	topSvgPos 	= parseInt(topSvgPos*hh/100);

	srcL = path + '/svg/' + $(this).attr('data-svgSrc');
	inSvg[type].svg(srcL, leftSvgPos, topSvgPos, widthSvg);
});
/*end create svg print*/


$('.printCarousel a.fill,.mobSliderPrint a.fill').click(function (event) {
	printWithFill = true;
	$('.textInputCanva').hide();
	$('.inputPrintTextMob').hide();
	$('.printSlides img').attr('src', $(this).find('img').attr('src'));
	event.preventDefault();
	clearCanvas();
	var type 		= 'front';
	printInCanvas 	= true;
	textInCanvas 	= false;
	// 
	leftSvgPos 	= $(this).attr('data-left-position');
	topSvgPos 	= $(this).attr('data-top-position');


	widthSvg  	= $(this).attr('data-width');
	ww = $('#FrontCanvas').innerWidth();
	hh = $('#FrontCanvas').innerHeight();
	
	widthSvg 	= parseInt(widthSvg*ww/100);
	leftSvgPos 	= parseInt(leftSvgPos*ww/100);
	topSvgPos 	= parseInt(topSvgPos*hh/100);

	srcL = path + '/svg/' + $(this).attr('data-svgSrc');
	inSvgWithFill[type].svg(srcL, leftSvgPos, topSvgPos, widthSvg);
}); 


var lines1 = [0,0,410,0];
var lines2 = [409,0,410,499];
var lines3 = [0,0,0,500];
var lines4 = [0,500,410,499];
var canvaW = window.innerWidth;

if (canvaW <= 1370  && canvaW>580) {
	lines1 = [0,0,290,0];
	lines2 = [289,0,290,399];
	lines3 = [0,0,0,400];
	lines4 = [0,400,290,399];
} else if (canvaW <=580 && canvaW > 440 ) {
	lines1 = [0,0,255,0];
	lines2 = [254,0,255,329];
	lines3 = [0,0,0,330];
	lines4 = [0,330,255,329];	
} else if (canvaW <= 440 && canvaW > 380) {
	lines1 = [0,0,223,0];
	lines2 = [222,0,223,299];
	lines3 = [0,0,0,300];
	lines4 = [0,300,223,299];	
}else if (canvaW <= 380  ) {
	lines1 = [0,0,188,0];
	lines2 = [187,0,188,219];
	lines3 = [0,0,0,220];
	lines4 = [0,220,188,219];	
}
/*border on canvas*/
line1 = new fabric.Line(lines1, {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});
line2 = new fabric.Line(lines2, {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});
line3 = new fabric.Line(lines3, {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});
line4 = new fabric.Line(lines4, {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});

setTimeout(clearCanvas,10);
function SetYourName(on) {
	if(on === true) {
		$('.input-append').show();
		// $('#position').show();
	} else if (on === false) {
		$('.input-append').hide();
		$('#position').hide()
	}
}
function clearCanvas() {
	canvas.clear().renderAll();
}

});
//doc ready

