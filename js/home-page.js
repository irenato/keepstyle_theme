/**
 * Created by Serg on 03.05.2016.
 */
var instagram_images = new Array();
var arr_count = 0;
$.ajax({
    method: 'GET',
    url: 'https://api.instagram.com/v1/users/409659213/media/recent/?access_token=3170185896.cf0499d.acd87a7ccf9344578d9d104379e996bf',
    dataType: "jsonp",
    jsonp: "callback",
    jsonpCallback: "jsonpcallback",
    success: function(response){
        //console.log(response);
        var images_obj = response;
        $.each(images_obj['data'], function(key, value){
            instagram_images[key] = [images_obj['data'][key]['link'], images_obj['data'][key]['images']['standard_resolution']['url']];
            arr_count = key;
        });

        changeGalleryImage();
    }
});

//window.setInterval(function(){
//    changeGalleryImage();
//}, 20000);


function changeGalleryImage(){
    var rand = randomListNoRepeat(0, arr_count);
    $('.insta-gallery').html('<div class="grid-sizer"></div><div class="grid-item"><a href="'+instagram_images[rand[0]][0]+'"><img src="'+instagram_images[rand[0]][1]+'" alt=""></a></div><div class="grid-item"><a href="'+instagram_images[rand[1]][0]+'"><img src="'+instagram_images[rand[1]][1]+'" alt=""></a></div><div class="grid-item grid-item--withTxt"><i class="icon-inst"></i><span class="line"></span><p>«Vestibulum sem metus, dictum ut turpis quis, viverra malesuada mauris.» </p><a class="subscribe" target="_blank" href="https://www.instagram.com/keep_style/">Подписаться</a><a  class="hash">#keepstyleshop</a><a href="#" class="clients">Наши клиенты</a></div><div class="grid-item "><a href="'+instagram_images[rand[2]][0]+'"><img src="'+instagram_images[rand[2]][1]+'" alt=""></a></div><div class="grid-item"><a href="'+instagram_images[rand[3]][0]+'"><img src="'+instagram_images[rand[3]][1]+'" alt=""></a></div><div class="grid-item"><a href="'+instagram_images[rand[4]][0]+'"><img src="'+instagram_images[rand[4]][1]+'" alt=""></a></div><div class="grid-item"><a href="'+instagram_images[rand[5]][0]+'"><img src="'+instagram_images[rand[5]][1]+'" alt=""></a></div><div class="grid-item"><a href="'+instagram_images[rand[6]][0]+'"><img src="'+instagram_images[rand[6]][1]+'" alt=""></a></div><div class="grid-item"><a href="'+instagram_images[rand[7]][0]+'"><img src="'+instagram_images[rand[7]][1]+'" alt=""></a></div><div class="grid-item"><a href="'+instagram_images[rand[8]][0]+'"><img src="'+instagram_images[rand[8]][1]+'" alt=""></a></div>').fadeIn(500);

    if ($('.grid').length) {
        var elem = document.querySelector('.grid');
        var msnry = new Masonry( elem, {
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true
        });
    }

}

function randomListNoRepeat (S, E)
{
    for (var O = [], R = [], j = 0; j < E - S + 1; j++) O [j] = S + j;
    while (O.length) R [R.length] = O.splice ((Math.random () * O.length), 1);
    return R;
}