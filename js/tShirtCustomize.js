/*
Created by Vladimir Razin;
E-mail: wowarazin@yandex.ru
 */

jQuery(document).ready(function($) {

  var thisColor10, thisColor30;
  var colThis;

  $('.colorPalette [type="radio"]').each(function(index, el) {
    var _this = $(this);

    if( _this.prop('checked') ) {
     var thisSpan = $(this).next('label').find('span');

     thisColor30 = thisSpan.css("background-color").replace(')', ', 0.3)').replace('rgb', 'rgba');
     thisColor10 = thisSpan.css("background-color").replace(')', ', 0.1)').replace('rgb', 'rgba');
     colThis = thisSpan.css("background-color");

     $('.valColor').css({
      background: colThis
    });

     $('.colorPalette span').css({
       // boxShadow: 'none'
     });

     thisSpan.css({ 
      boxShadow: '0px 0px 0px 10px ' + thisColor30
    });

     $('.wrappLeft').css({
      background: thisColor10
    });

     $('.printTexture input').css({
       boxShadow: 'none '
     });

     $('input:checked + label>span' , '.printTexture ').css({
       boxShadow: '0px 0px 0px 10px ' + thisColor30
     });

     $('.mobileControl .leftSide, .mobileControl .rightSide').css({
      background: thisColor10
    });

   }

 });
  $('.colorPalette span').on({
    mouseover: function() {
      var thisSpan = $(this);
      var thisColor10 = thisSpan.css("background-color").replace(')', ', 0.1)').replace('rgb', 'rgba');
      $('.wrappLeft').css({
        background: thisColor10
      });
      $(this).css({ 
        boxShadow: '0px 0px 0px 10px ' + thisColor30
      });
    }, mouseout: function() {
      if($(this).parent().prev().prop('checked') != true ) {
        $(this).css({ 
          boxShadow: 'none'
        });
      }

    }
  });

  $('.colorPalette [type="radio"]').on('change', function() {
    if( $(this).prop('checked') ) {
      var thisSpan = $(this).next('label').find('span');

      thisColor30 = thisSpan.css("background-color").replace(')', ', 0.3)').replace('rgb', 'rgba');
      thisColor10 = thisSpan.css("background-color").replace(')', ', 0.1)').replace('rgb', 'rgba');
      colThis = thisSpan.css("background-color");

      $('.valColor').css({
        background: colThis
      });

      $('.colorPalette span').css({
       boxShadow: 'none'
     });

      thisSpan.css({ 
        boxShadow: '0px 0px 0px 10px ' + thisColor30
      });

      $('.wrappLeft').css({
        background: thisColor10
      });

      $('.printTexture input').css({
       boxShadow: 'none '
     });

      $('input:checked + label>span' , '.printTexture ').css({
       boxShadow: '0px 0px 0px 10px ' + thisColor30
     });

      $('.mobileControl .leftSide, .mobileControl .rightSide').css({
        background: thisColor10
      });
    }
  });

  $('.thirdMobTab .printTexture [type="radio"]').each(function(index, el) {
   if( $(this).prop('checked') ) {
     var thisSpan = $(this).next('label').find('span');
     colThis = thisSpan.css("background-image");
     $('.mobPrintCol').css({
      backgroundImage: colThis,
      backgroundSize: 'cover'
    })
   }
 })

  $('.printTexture span').on({
    mouseover: function() {
      $(this).css({ 
        boxShadow: '0px 0px 0px 10px ' + thisColor30
      });
    }, mouseout: function() {
      if($(this).parent().prev().prop('checked') != true ) {
        $(this).css({ 
          boxShadow: 'none'
        });
      }
      
    }
  });
  $('.printTexture [type="radio"]').on('change', function() {
    if( $(this).prop('checked') ) {
      var thisSpan = $(this).next('label').find('span');
     
      colThis = thisSpan.css("background-image");
      $('.mobPrintCol').css({
        backgroundImage: colThis,
        backgroundSize: 'cover'
      });
      $('.printTexture span').css({
       boxShadow: 'none'
     });
      thisSpan.css({ 
        boxShadow: '0px 0px 0px 10px ' + thisColor30
      });
    }
  })

  /**/


  /*printSlides*/
  var owl = $('.printCarousel .carouselItems');
  owl.owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
      0:{
        items:1,
        nav:true
      },
      600:{
        items:3,
        nav:false
      },
      1200:{
        items:3
      },
      1366:{
        items:5,
        nav:false,
        loop:false,
        margin: 50
      }
    }
  })
  var carousel = owl.data('owlCarousel');


  $('.printCarousel .rightSlide').click(function() {
    owl.trigger('next.owl.carousel');
  });
  $('.printCarousel .leftSlide').click(function() {
    owl.trigger('prev.owl.carousel');
  });


  var fC= true;

  var ww = $(window).width();
  $('.printSlides img').click(function(){
    if(ww>=1200) {
      if(!fC) {
        var carousel = owl.data('owlCarousel');
        carousel._width = $('.printCarousel .owl-carousel').width();
        carousel.invalidate('width');
        carousel.refresh();
        owlAligner();
        $('.printSlides').css('background', 'transparent');
        $('.printCarousel').css({
         transform: 'translate(-100%)'
       });
        fC = true;
      } else {
        $('.printSlides').css({
          'background':'#F1D9E4'
        });
        $('.printCarousel').css({
         transform: 'translate(0)'
       });
        fC = false;
      }
    }

  });


  $(document).click(function(e) {
    if ( $(e.target).closest('.printCarousel').length === 0 && $(e.target).closest('.printSlides img').length === 0) {
     $('.printSlides').css('background', 'transparent');
     $('.printCarousel').css({
       transform: 'translate(-100%)'
     });
     fC = true;
   }
 });
  // $('body').click(function (e) {
  //   var printCarousel = $('.printCarousel').css('transform');
  //   if (printCarousel == 'matrix(1, 0, 0, 1, 0, 0)'  ) {
  //       event.stopPropagation();
  //     $('.printCarousel').css({
  //      // transform: 'translate(-100%)'
  //    });
  //      $('.printSlides').css('background', 'transparent');
  //      fC = false;
  //   }  
  // });
$('.printSlides img').on({
  mouseover: function() {
    $(this).parents('.printSlides').addClass('hover')
  },
  mouseout: function() {
   $(this).parents('.printSlides').removeClass('hover')
 }
})


$('.printCarousel a,.printCarousel .your-name').click(function(event) {
  event.preventDefault();
 $('.printSlides').css('background', 'transparent');
 $('.printCarousel').css({
   transform: 'translate(-100%)'
 });
 fC = !fC;
});


$('.printInformation .sizeAdult li span').click(function(event) {
  $('.printInformation .sizeAdult li').removeClass('active');
  $(this).parent().addClass('active');
});
$('.printInformation .sizeChild li span').click(function(event) {
  $('.printInformation  .sizeChild li').removeClass('active');
  $(this).parent().addClass('active');
});
$('.mobileControl .sizeAdult li span').click(function(event) {
  $('.mobileControl .sizeAdult li').removeClass('active');
  $(this).parent().addClass('active');
});
$('.mobileControl .sizeChild li span').click(function(event) {
  $('.mobileControl  .sizeChild li').removeClass('active');
  $(this).parent().addClass('active');
});
});

/* <----------   */
function printSlidesHeight() {
  $('.printCarousel .carouselItems').height($('.printSlides ').innerHeight());
}

jQuery(document).ready(printSlidesHeight);
jQuery(window).resize(printSlidesHeight);
/* ----------> */

/* <----------   */
function printCarouselPosition() {
 var topPosSlider = $('.printSlides').position().top;
 $('.printCarousel').css({
  top: topPosSlider - 1 + 'px'
})
}

jQuery(document).ready(printCarouselPosition);
jQuery(window).resize(printCarouselPosition);
jQuery('.tShirt .left-controll .wrappLeft').scroll(printCarouselPosition);
/* ----------> */



/* <----------   */
function constSideheight() {
  $('.tShirt .left-controll .wrappLeft').height($(window).height() - $('header').innerHeight());
  $('.tShirt .left-controll .wrappLeft').niceScroll({
    cursorcolor: "#ccc",
    cursorwidth: '2.5px',
    bouncescroll: true,
    cursorborderradius: 0,
    horizrailenabled:false,
    touchbehavior: false,
    preventmultitouchscrolling: false,
    cursordragontouch: true,
    zIndex: 0,
    railoffset:{"top": -$('header').innerHeight(),"left":0}
  });
}
jQuery(document).ready(constSideheight);
jQuery(window).resize(constSideheight);
/* ----------> */


function reloadLocation() {
  location.reload();
}
// jQuery(window).resize(reloadLocation);

jQuery(document).ready(function($) {
  /*mobile constructor control*/


  /*параметры: тип*/

  $('.firstMobTab .valShirt').click(function(event) {
   $('.firstMobTab .leftSide').css('width', '100%' );
   $('.firstMobTab .rightSide').hide();
   $('.firstMobTab .valShirt').hide();
   $('.firstMobTab .title').hide();
   $('.firstMobTab .listShirt').show();
 });

  $('.firstMobTab .listShirt li').click(function(event) {
    var favClass = $(this).find('span').attr('class');
    $('.firstMobTab .leftSide span i').removeAttr('class');
    $('.firstMobTab .leftSide span i').attr('class', favClass);
    $('.firstMobTab .leftSide').css('width', '50%' );
    $('.firstMobTab .rightSide').show();
    $('.firstMobTab .valShirt').css('display', 'inline-block');
    $('.firstMobTab .title').show();
    $('.firstMobTab .listShirt').hide();
  });


  $('.firstMobTab .valGender').click(function(event) {
   $('.firstMobTab .rightSide').css('width', '100%' );
   $('.firstMobTab .leftSide').hide();
   $('.firstMobTab .valGender').hide();
   $('.firstMobTab .title').hide();
   $('.firstMobTab .gender').show();
 });


  $('.firstMobTab .gender label').click(function(event) {
    var favClass = $(this).find('span').attr('class');
    $('.firstMobTab .rightSide span i').removeAttr('class');
    $('.firstMobTab .rightSide span i').attr('class', favClass);
    $('.firstMobTab .rightSide').css('width', '50%' );
    $('.firstMobTab .leftSide').show();
    $('.firstMobTab .valGender').show();
    $('.firstMobTab .title').show();
    $('.firstMobTab .gender').hide();
  });


  /*при нажатии на тип сбрасываем на дефолт*/
  $('.mobType').click(function(event) {
    $('.addToBaskMob a').find('i').attr('class', 'icon-cartAdd');
    event.preventDefault();
    $('.tabsControl a').removeClass('active');
    $(this).addClass('active');
    $('.firstMobTab').addClass('active');
    $('.firstMobTab .title').show();
    $('.firstMobTab .leftSide').show();
    $('.firstMobTab .rightSide').show();
    $('.firstMobTab .valGender').show();
    $('.firstMobTab .valShirt').css('display', 'inline-block');
    $('.firstMobTab .listShirt').hide();
    $('.firstMobTab .gender').hide();
    $('.secondMobTab').removeClass('active');
    $('.thirdMobTab').removeClass('active');
    $('.firstMobTab').find('.rightSide').css({
      width: '50%'
    });
    $('.firstMobTab').find('.leftSide').css({
      width: '50%'
    });
    
  });
  /*параметры: размер*/
  $('.secondMobTab .valSize').click(function(event) {
    event.preventDefault();
    var thisP = $(this).parents('.leftSide');
    thisP.css({
      width: '100%' 
    });
    thisP.siblings('.rightSide').hide();
    $(this).hide();
    $('.secondMobTab .sizeAdult').show();
    $('.secondMobTab .title').hide();
  });

  $('.secondMobTab .valSizeChild').click(function(event) {
    event.preventDefault();
    var thisP = $(this).parents('.leftSide');
    thisP.css({
      width: '100%' 
    });
    thisP.siblings('.rightSide').hide();
    $(this).hide();
    $('.secondMobTab .sizeChild').show();
    $('.secondMobTab .title').hide();
  });


  $('.secondMobTab .sizeAdult li').click(function(event) {
    var thisP = $(this).parents('.leftSide');
    $('.mobileControl .sizeAdult').hide();
    $('.secondMobTab .title').show();
    thisP.css({
      width: '50%' 
    });
    thisP.siblings('.rightSide').show();
    $(this).parents('.mobAligner').find('.valSize').text($(this).text());
    $(this).parents('.mobAligner').find('.valSize').show()

  });

  $('.secondMobTab .sizeChild li').click(function(event) {
    var thisP = $(this).parents('.leftSide');
    $('.mobileControl .sizeChild').hide();
    $('.secondMobTab .title').show();
    thisP.css({
      width: '50%' 
    });
    thisP.siblings('.rightSide').show();
    $(this).parents('.mobAligner').find('.valSizeChild').text($(this).text());
    $(this).parents('.mobAligner').find('.valSizeChild').show()

  });


  /*параметры: цвет*/
  $('.secondMobTab .valColor').click(function(event) {
    event.preventDefault();
    var thisP = $(this).parents('.rightSide');
    thisP.css({
      width: '100%' 
    });
    thisP.prev('.leftSide').hide();
    $(this).hide();
    $('.secondMobTab .colorPalette').show();
    $('.secondMobTab .title').hide();
  });

  $('.secondMobTab .colorPalette label').click(function(event) {
    var thisP = $(this).parents('.rightSide');
    $('.secondMobTab .title').show();
    $(this).parents('.colorPalette ').hide();
    thisP.prev('.leftSide').show();

    $('.secondMobTab .valColor').show();
    thisP.css({
      width: '50%' 
    });
  });



  $('.mobileControl').click(function(event) {
   $('.addToBaskMob a').find('i').attr('class', 'icon-cartAdd');
 });

  var age = 'adult';

  $('.gender input').on('change', function() {
    if( $(this).prop('checked') ) {
      age = $(this).data('gender');
    }
  });
  $('.tableSize a ').click(function(event) {
    event.preventDefault();
    $('.modalAdult,.modalChild').css({
     transform: 'translate(-50%,-200%)',
     top: '0'
   })
    if(age === 'adult') {
     $('.modalAdult').css({
       transform: 'translate(-50%,-50%)',
       top: '50%'
     })
   } else {
    $('.modalChild').css({
     transform: 'translate(-50%,-50%)',
     top: '50%'
   })
  }
});

  $('.modalChild .icon-close,.modalAdult .icon-close').click(function(event) {
    $('.modalAdult,.modalChild').css({
     transform: 'translate(-50%,-200%)',
     top: '0'
   })
  });  
  $(document).on('mouseup touchstart', function (e) {
    var modalSize = $(".modalChild,.modalAdult");

    if (!modalSize.is(e.target) && modalSize.has(e.target).length === 0) {
      modalSize.css({
       transform: 'translate(-50%,-200%)',
       top: '0'
     })
    }
  });

  $('.composition_title span').click(function(event) {
   $('.composition_title span').removeClass('active');
   $(this).addClass('active');
 });
  $('.composition_title .com1').click(function(event) {
   $('.composition_tab').hide();
   $('.composition_tab_1').show();
 });
  $('.composition_title .com2').click(function(event) {
   $('.composition_tab').hide();
   $('.composition_tab_2').show();
 });

  /*при нажатии на параметры сбрасываем на дефолт*/
  $('.mobParam').click(function(event) {
    event.preventDefault();
    if(age === 'adult') {
      $('.secondMobTab .valSize').show();
      $('.secondMobTab .valSizeChild').hide();
    } else {
      $('.secondMobTab .valSizeChild').show();
      $('.secondMobTab .valSize').hide();
    }
    $('.addToBaskMob a').find('i').attr('class', 'icon-cartAdd');
    $('.tabsControl li a').removeClass('acive');
    $('.tabsControl a').removeClass('active');
    $(this).addClass('active');
    $('.secondMobTab').find('.rightSide').css({
      width: '50%'
    });
    $('.secondMobTab').find('.leftSide').css({
      width: '50%'
    });
    $('.secondMobTab .leftSide').show();
    $('.secondMobTab .rightSide').show();
    $('.secondMobTab .valColor').show();
    
    $('.secondMobTab .title').show();
    $('.secondMobTab .colorPalette').hide();
    $('.secondMobTab .sizeAdult').hide();
    $('.secondMobTab .sizeAdultChild').hide();

    $('.secondMobTab').addClass('active');
    $('.firstMobTab').removeClass('active');
    $('.thirdMobTab').removeClass('active');
  });

/*при нажатии на принт сбрасываем на дефолт*/
$('.mobPrint').click(function(event) {
  $('.addToBaskMob a').find('i').attr('class', 'icon-cartAdd');
  event.preventDefault();
  $('.tabsControl a').removeClass('active');
  $(this).addClass('active');
  $('.secondMobTab').removeClass('active');
  $('.firstMobTab').removeClass('active');
  $('.thirdMobTab').addClass('active');


  $('.thirdMobTab .printTexture').hide();
  $('.thirdMobTab .leftSide').show();

  $('.thirdMobTab').find('.leftSide').css('width', '50%');
  $('.thirdMobTab').find('.rightSide').css('width', '50%');
  $('.thirdMobTab').find('.mobPrintCol').show();
  $('.thirdMobTab .printSlides').show();
  $('.thirdMobTab .title').show();
  $('.thirdMobTab .mobSliderPrint').hide();
  $('.thirdMobTab .rightSide').show();
});

$('.thirdMobTab .leftSide .contentInner img').click(function(){
  var owl2 = $(' .mobSliderPrint .carouselItems');
  owl2.owlCarousel({
    loop:true,
    margin:10,
    items: 5,
    responsiveClass:true,
    responsive:{
      0:{
        items:1,
        margin: 0
      },
      440:{
        items:2,
        margin:0
      },
      600:{
        items:3,
        nav:false,
        loop:false,
        margin: 50
      }
    }
  });

  $('.thirdMobTab .rightSlide').click(function() {
    owl2.trigger('next.owl.carousel');
  });
  $('.thirdMobTab .leftSlide').click(function() {
    owl2.trigger('prev.owl.carousel');
  });

  $('.thirdMobTab .mobSliderPrint').show();
  $('.thirdMobTab .printSlides').hide();
  $('.thirdMobTab .removePrint').hide();
  $('.thirdMobTab .title').hide();
  $('.thirdMobTab .leftSide').css('width', '100%' );
  $('.thirdMobTab .rightSide').hide();
  var carousel = owl2.data('owlCarousel');
  carousel._width = $('.mobSliderPrint .owl-carousel').width();
  carousel.invalidate('width');
  carousel.refresh();
  owlAligner();
});
/*принт: цвет принта*/

$('.thirdMobTab .mobPrintCol').click(function(event) {
 event.preventDefault();
 $('.thirdMobTab .leftSide').hide();
 $('.thirdMobTab .title').hide();
 $('.thirdMobTab .rightSide').css('width', '100%');
 $(this).hide();
 $('.thirdMobTab .printTexture').show();

});

$('.thirdMobTab .printTexture label').click(function(event) {
  $('.thirdMobTab .printTexture').hide();
  $('.thirdMobTab .title').show();
  $('.thirdMobTab .mobPrintCol').show();
  $('.thirdMobTab .leftSide').show();
  $('.thirdMobTab .rightSide').css('width', '50%');
});

$('.thirdMobTab .mobSliderPrint a,.thirdMobTab .mobSliderPrint .your-name').click(function(event) {
 $('.thirdMobTab .mobSliderPrint').hide();
 $('.thirdMobTab .removePrint').show();
 $('.printTexture').hide();
 $('.thirdMobTab .rightSide').show();
 $('.thirdMobTab .leftSide').css('width', '50%');
 $('.thirdMobTab .printSlides').show();
 $('.thirdMobTab .title').show();
});
/*mobile constructor control*/
$('.quantityTShirt .minVal').click(function (e) {
  var val = $(this).siblings('.counter').text();

  val = parseInt(val);
  val--;
  if(val < 1) {
    val = 1;
  }
  $(this).siblings('.counter').text(val)
});

$('.quantityTShirt .maxVal').click(function (e) {
  var val = $(this).siblings('.counter').text();

  val = parseInt(val);
  val++;
  if(val > 99) {
    val = 99;
  }
  $(this).siblings('.counter').text(val)
});


$('.infoBtn i').click(function(event) {
  $('.composition').css({
    transform: 'translate(0)'
  })
});
$('.close_comp').click(function(event) {
  $('.composition').css({
    transform: 'translate(120%)'
  })
});


$('.addToBaskMob a').click(function(event) {
  event.preventDefault();

  $(this).find('i').attr('class', 'icon-cartOk')
});
});


function owlAligner() {

  var maxH = 0;
  var maxW = 0;
  $('.printCarousel div.owl-item > div').each(function(f) {
    if ($(this).height() > maxH) { maxH = $(this).height() };
    if ($(this).width() > maxW) { maxW = $(this).width()};
  });
  $('.printCarousel div.owl-item > div')
  .height(maxH)
  .width(maxW);
}
$(window).load(owlAligner);

var ww = window.innerWidth;

var canvas = document.getElementById('FrontCanvas');
var canW = 410, canH = 500;

if(ww <= 1370 && ww>580) {
  canW = 290;
  canH = 400;
  // alert(480)
} else if (ww <= 580 && ww > 440 ) {
  canW = 255;
  canH = 330;
} else if (ww <= 480 && ww > 380) {
  canW = 223;
  canH = 300;
}else if (ww <= 380) {
  canW = 188;
  canH = 220;
}

canvas.width  = canW;
canvas.height = canH;

var w = 0;
$( window ).load( function(){
  w = $( window ).width();
});
$( window ).resize( function(){
  if( w != $( window ).width() ){
    this.location.reload(false);
    w = $( window ).width();
    delete w;
  }
});





$(window).load(function(){
    var loc = location.search;
    if(loc =="?1") {
        if($(window).width()>=1200) {
            $('.left-controll .listShirt [data-shirt="shirt"]').trigger('click');
        } else {
            $('.mobileControl .listShirt [data-shirt="shirt"]').trigger('click');
        }
    }
})