// Ajax functions
// Gift Form
    $('form.giftForm').submit(function(e){
        e.preventDefault();
        var action = 0;
        $(".giftForm .giftData").each(function() {
            if($(this).val() == "") {
                $(this).parent().addClass('error');
                action++;
            } else $(this).parent().removeClass('error');
        });
        var price = $('.giftForm .resultSelect').text();
        if(price == ""){
            $('.giftForm .resultSelect').parent().parent().addClass('error');
            action++;
        } else $('.giftForm .resultSelect').parent().parent().removeClass('error');

        var message = $('.giftForm textarea').val();

        if(message == ""){
            $('.giftForm textarea').addClass('error');
            action++;
        }else $('.giftForm textarea').removeClass('error');

        if(action == 0){

            var basePrice = $('.giftForm .resultSelect').data('value');
            var name = $('.giftForm input[name=name]').val();
            var email = $('.giftForm input[name=email]').val();

            var text = 'Подарочная сумма: '+price+', Кому: '+name+' ('+email+'), Сообщение: '+message;

            var post = {
                action: 'addGiftCard',
                text: text,
                price: basePrice

            };

            $.post(sendAjax.url, post, function(response) {
                window.location = response;
            });
        }


    });

// CallBack Footer
    $('form.callBack').submit(function(e){
        e.preventDefault();
        var action = 0;
        $("form.callBack input[type=text]").each(function() {
            if($(this).val() == ""){
                $(this).parent().addClass('error');
                action++;
            }else $(this).parent().removeClass('error');

        });

        if(action == 0){
            var data = $(this).serializeArray();
            var post = {
                action: 'callBack',
                data: data
            };

            $.post(sendAjax.url, post, function(response) {
               window.location = response;
            });
        }

    });

/*removeProduct*/
    $('.removeProduct').click(function() {
        var post = {
            action: 'deleteThisProduct',
            id: $(this).attr('data-id')
        };

        $.post(sendAjax.url, post, function(response) {
            location.reload();
        });

        $(this).parent().next('.lineBasket').remove();
        $(this).parent().remove();

    });

    $('.enterCert').click(function(){
        var gift_code = $(this).prev().find('input').val();
        var total_price = parseInt($("#product-total-price").text());
        if(gift_code != ""){

            var post = {
                action: 'getGiftCertificate',
                code: gift_code,
                total: total_price
            };

            $.post(sendAjax.url, post, function(response) {
                if(response == 'none'){
                    $('.cartErrorTitle').show();
                    setTimeout(function(){$('.cartErrorTitle').hide()}, 5000);
                }else{
                    $("#gift-price").text(-response);
                    total_price = total_price - response;
                    $(".counter-total-price").text(total_price);
                }
            });

        }
    });

    $('#next-tab').click(function(e){
        var counter = [];
        var cnt = 0;
        $(".counter").each(function() {
            counter[cnt] = [$(this).attr('data-id'), $(this).text()];
            cnt++;
        });

        var post = {
            action: 'changeProductsCount',
            data: counter
        };

        $.post(sendAjax.url, post, function(response) {
            if(response == 'none')
                location.reload();
            else {
                $(".counter-total-price").text(response);
                $("div.reg").removeClass('active');
                $("div.delivery").addClass('active');
                $('.regTab').hide();
                $('.lineHidden-last').css({
                    background: '#0cf'
                });
                $('.deliveryTab').show();
            }
        });


    });

    $('form.main-information').submit(function(e){
        e.preventDefault();

        var cnt = getMainInformation();
        if(cnt == 0){
             var post = {
                 action: 'generatePaymentForms',
                 info: $('form.main-information').serializeArray(),
                 delivery: $('.deliveryResult').attr('data-value')
             };

             $.post(sendAjax.url, post, function(response) {
                 $('#paymentForms').html(response);
                 $("div.delivery").removeClass('active');
                 $("div.payment").addClass('active');
                 $('.deliveryTab').hide();
                 $('.lineHidden-last').css({
                     background: '#0cf'
                 });
                 $('.paymentTab').show();
                 $('.counter-total-price').html($('input[name=delivery-price]').val());
             });

        }else $('div.mainErrorTitle').addClass('onError');
    });

    // Payment Form senders
    $('#payPalSubmit').click(function(e){
        e.preventDefault();
        $('#payPalForm').submit();
    });

    $('#liqPaySubmit').click(function(e){
        e.preventDefault();
        $('#liqPayForm').submit();
    });

    $('form.payment-form').submit(function(e){
        e.preventDefault();

        var post = {
            action: 'createNewOrder',
            status: 0
        };
        $.post(sendAjax.url, post, function(response) {
            window.location = response;
        });

    });

    $('.currencyList a').click(function(e){
        e.preventDefault();
        var active = $(this).attr('data-id');
        var post = {
            action: 'setActiveCurrency',
            id: active
        };

        $.post(sendAjax.url, post, function() {
            location.reload();
        });

    });

    //Upload Comment Avatar
    $('#dropzone input').on('change', function(e) {
        var file = this.files[0];

        $('#dropzone').removeClass('hover');

        if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
            // return alert('Выберите фото.');
        }

        $('#dropzone').addClass('dropped');
        $('#dropzone img').remove();

        if ((/^image\/(gif|png|jpeg|jpg)$/i).test(file.type)) {
            var formData = new FormData();
            formData.append('action', 'actionUploadedFile');
            formData.append('file', file);
            var request = $.ajax({
                data: formData,
                url: sendAjax.url,
                type: 'post',
                dataType: 'html',
                contentType: false,
                processData: false
            });

            request.done(function(response) {
                $('.dialog input[type=submit]').attr('data-image', response);
                $img = $('<img />').attr('src', response).fadeIn();
                $img.css({
                    maxWidth:'100%',
                    display : 'inline-block',
                    verticalAlign: 'middle'
                });
                $('#dropzone .dropzone_title').addClass('lh');
                $('#dropzone .dropzone_title').html($img);
            });


        } else {
            alert('Выберите фото');

        }
    });

    $('.dialog input[type=submit]').click(function(e){
        var id = $(this).attr('data-id');
        var img = $(this).attr('data-image');
        var name = $('.dialog input[name=first-name]').val();
        var comment = $('.dialog textarea').val()
        if(img && name && comment){
            var post = {
                action: 'createNewComment',
                data: {id : id, img : img, name: name, comment: comment},
            };

            $.post(sendAjax.url, post, function() {
                location.reload();
            });
        }else alert('Enter All Fields');

    });


//End Ajax functions
$country_city = 0;

$( ".countrySelect li").click(function() {
    var country = $(this).text();
    if(country == 'Россия' || country == 'Russia'){
        $country_city = 1;
    }else if(country == 'Украина' || country == 'Ukraine'){
        $country_city = 2;
    }else{
        $country_city = 0;
    }

    $( ".deliverySelect").html('');
    $('.deliveryResult').text('');
    $('.deliveryTitle').removeAttr('style');
});

$( "select[name=delivery-method]").change(function() {

});

$( "input[name=city]").change(function() {
    var city = $(this).val();
    var post = {
        action: 'getDeliveryMethod',
        country: $country_city,
        city: city
    };

    $.post(sendAjax.url, post, function(response) {
        //alert(response);
        $( ".deliverySelect").html(response);
        $('.deliveryResult').text('');
        $('.deliveryTitle').removeAttr('style');
    });

    $('select[name=delivery-method]').trigger('focusout');

});

function addToBasket(controllClass, sizeShirtPosition){

    var type = $('.flip').data('type');
    var shirtName = $('.printInformation .title').text();

    var imgCanva = $('#mainCanvas img').attr('src');
    var age = 'adult';
    var size = 's';
    var obj = canvas.toJSON();
    var objLength = obj.objects.length;

    var printTextura = "";
    var printBasePrice = 0;

    var countShirt = $('.countTShirt .counter').text();
    var price = $(controllClass + ' .listShirt li.active').data('baseprice');
    price = parseInt(price);

    if (type == 'front') {
        imgCanva = $('#mainCanvas img').data('front');
    } else {
        imgCanva = $('#mainCanvas img').data('back');
    }

    $(controllClass + ' .gender input').each(function () {
        if ($(this).prop('checked')) {
            age = $(this).data('gender');
            //alert(age);
        }
    });


    $(controllClass + ' .printTexture input').each(function () {
        if ($(this).prop('checked')) {
            printTextura = $(this).next('label').find('span').css('background-image');
            printTextura = '"' + printTextura + '"';

            printBasePrice = $(this).next('label').find('span').attr('data-basePrice');
            printBasePrice = parseInt(printBasePrice);
        }
    });

    var total = printBasePrice + price;

    if (age == 'adult') {
        size = $(sizeShirtPosition + ' .sizeAdult li.active span').text();
       } else {
        size = $(sizeShirtPosition + ' .sizeChild li.active span').text();
    }


    var imgSrc = '';
    var printText = '';

    if (objLength == '1') {
        imgSrc = $(controllClass + ' .printSlides img').attr('src');

    } else if (objLength == '2') {
        printText = obj.objects[0].text + '|' + obj.objects[1].text;

    }
    if (printTextura != '') {
        var temp = printTextura.slice(1,6);
        if(temp == "url(h")
            printTextura = printTextura.slice(5, -2);
        else printTextura = printTextura.slice(6, -3);
    }

    var product = {
        name: shirtName,
        image: imgCanva,
        size: size,
        sex: age,
        status: objLength,
        printImg: imgSrc,
        print_color: printTextura,
        printText: printText,
        price: total,
        count: countShirt
    }


    if (product.status == 0) {
        alert('выберите принт');
    } else if (product.status == 2 && $(controllClass + ' .text-string').val() == '')  {
        alert('Введите текст');
    }else if(product.status == 2  && $(controllClass + ' .number-string').val() == '') {
        alert('Введите номер');
    }else if(product.print_color == '') {
        alert('Выберите цвет принта');
    }
    else {

        //alert(product.print_color);
        var post = {
            action: 'addNewProduct',
            product: product
        };

        $.post(sendAjax.url, post, function(response) {
            window.location = response;
        });
    }
}
/*const*/
$('.addToBasket a').click(function(event) {
    event.preventDefault();
    if($(window).width() >= 1200) {
        addToBasket('.left-controll', '.printInformation');
        //console.log(age);
    } else {
        //console.log(age);
        addToBasket('.mobileControl', '.mobileControl');

    }
});

function getMainInformation(){
    var cnt = 0;
    $(".current-value").each(function() {
        if($(this).attr('name') == 'email' || $(this).attr('name') == 'first-name' || $(this).attr('name') == 'phone' || $(this).attr('name') == 'city' || $(this).attr('name') == 'delivery-details')
            if($(this).val() == "") {
                $(this).parent().addClass('error');
                cnt++;
            } else $(this).parent().removeClass('error');
    });

    $(".resultSelect").each(function() {
        if($(this).text() == ""){
            $(this).parent().parent().addClass('error');
            cnt++;
        } else $(this).parent().parent().removeClass('error');
    });

    return cnt;
}

function recalculationPrice(){
    var price = 0;
    $(".product-total-price").each(function() {
        price += parseInt($(this).text());
    });

    $('#product-total-price').text(price);

    price += parseInt($('#gift-price').text());

    $('.counter-total-price').text(price);

}