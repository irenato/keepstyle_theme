jQuery(document).ready(function($) {
    var rsClick = 1;

    // $('.customeSelect').each(function(index, el) {
    // 	var thisHeight = $(this).innerHeight();

    // 	$(this).css({
    // 		bottom: -thisHeight + 'px'
    // 	})
    // });

    $('body').delegate('.customeSelect li', 'click', function(event) {
        var thisValue = $(this).text();
        var dataValue = $(this).attr('data-value');

        $(this).parent().css({
            opacity: 0,
            visibility: 'hidden'
        });
        rsClick = !rsClick;

        $(this).parent('.customeSelect').prev().text(thisValue);
        $(this).parents('.group').find('input').val(thisValue);
        $(this).parent('.customeSelect').prev().attr('data-value', dataValue);
    });

    $('.resultSelect').click(function(event) {
        $('.customeSelect').css({
            opacity: 0,
            visibility: 'hidden'
        });
        $('.resultSelect ').each(function(){
            if($(this).is(':empty')) {
                $(this).prev().css( {
                    left: '15px',
                    top: '4px'
                })
            }
        })
        rsClick = 1;
        if(rsClick) {
            $(this).siblings('.customeSelect').css({
                opacity: 1,
                visibility: 'visible'
            });
            var titleWidth = $(this).prev().width();
            if($(window).width()>992) {
                $(this).prev().css({
                    left: -titleWidth - 15 + 'px',
                    top: '4px'
                });
            } else {
                $(this).prev().css({
                    left: '15px',
                    top: '-17px'
                });
            }

            rsClick = !rsClick;
        } else if (!rsClick && $(this).is(':empty')) {
            $(this).siblings('.customeSelect').css({
                opacity: 0,
                visibility: 'hidden'
            });
            if($(window).width()>992) {
                $(this).prev().css({
                    left: '15px',
                    top: '4px'
                });
            } else {
                $(this).prev().css({
                    left: '15px',
                    top: '4px'
                });
            }

            rsClick = !rsClick;
        }


    });

    $(document).on('mouseup touchstart', function (e) {
        var resultSelect = $( ".selectParent", this);

        if (!resultSelect.is(e.target) && resultSelect.has(e.target).length === 0) {
            $('.customeSelect').css({
                opacity: 0,
                visibility: 'hidden'
            });
            $('.resultSelect ').each(function(){
                if($(this).is(':empty')) {
                    $(this).prev().css( {
                        left: '15px',
                        top: '4px'
                    })
                }
            });

            rsClick = 1;
        }
    });
});