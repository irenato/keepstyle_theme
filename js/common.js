/*
Created by Vladimir Razin;
E-mail: wowarazin@yandex.ru
 */

$(function() {
	var $window = $(window),
	header   = $('header');
	
	// floatSocialMenu
	function floatSocialMenu() {
		if($('.we-in-social').length) {
			$window.scrollTop() > header.position().top ? header.addClass('sticky') : header.removeClass('sticky');
		}else return;
		

	}

	$(document).ready(floatSocialMenu);
	$window.resize(floatSocialMenu);
	$window.scroll(floatSocialMenu);
	// floatSocialMenu


	// scrollToFadeImg
	function scrollToFadeImg () {
		if ( $window.scrollTop() >= ($window.height() - $('header').height())) {
			setTimeout(fadeImage, 200)
		}
	}
	
	$(document).ready(scrollToFadeImg);
	$window.resize(scrollToFadeImg);
	$window.scroll(scrollToFadeImg);
	// scrollToFadeImg
	// 
	// 
	$('#scrollDown i, #scrollDown p').click(function(e) {
		var $first_screen = $window.height();

		$("html, body").stop().animate({ 
			scrollTop:  $first_screen -$('header').height() +  'px' 
		}, 800);
		setTimeout(fadeImage, 800);

		if( $window.width()<1200) {
			$("html, body").stop()
		}
	});

	function fadeImage () {
		$('.slideImage').css({
			transform: 'translateY(0)',
			opacity: 1
		});
	}


	$(window).load(function(){
		if ($('.grid').length) {
			var elem = document.querySelector('.grid');
			var msnry = new Masonry( elem, {
				itemSelector: '.grid-item',
				columnWidth: '.grid-sizer',
				percentPosition: true
			});	
		}
		
	})

	$(document).ready(marginFromHeader);
	$(window).resize(marginFromHeader);
});

function parallaxFooter() {
	$('footer').prev().css({
		marginBottom: $('footer').outerHeight() + 'px',
		boxShadow: 'rgba(0, 0, 0, 0.8) 0px 0px 9px -2px' ,
		background: 'rgb(243, 246, 248)',
		position: 'relative',
		zIndex: 10
	});
	$('footer').css({
		position: 'fixed',
		width: '100%',
		zIndex: 0,
		bottom: 0,
		left: 0,
		right: 0
	});
}

function parallaxOff() {
	var hh = $('header').innerHeight();
	// $('footer').prev().css({
	// 	marginBottom: hh +'px' 
	// });
	$('footer').css({
		position: 'relative',
		width: '100%'
	});	
}


function marginFromHeader() {
	if($('.we-in-social').length <=0) {
		var hh = $('header').innerHeight();
		var whoM = $('header').next();
		whoM.css({
			position: 'relative',
			marginTop: hh + 'px' 	
		})
	}
}

$(document).ready(function() {
	if ($(window).height() >  $('footer').outerHeight() && $(window).width()> 1200) {
		parallaxFooter();
	}else {
		parallaxOff()
	}
});

$(window).resize(function() {
	if ($(window).height() >  $('footer').outerHeight() && $(window).width()> 1200) {
		parallaxFooter();
	}else {
		parallaxOff()
	}
});

var setImage = function () {
	$('.MaterialPage').each(function(){
		var _this 	= $(this),
		_thisBG = _this.data('img'),
		path 	= '../build/';

		_this.css({
			backgroundImage: 'url('+ _thisBG + ')',
			backgroundSize : 'cover'
		});
	})	
}

$(document).ready(function() {
	if ( $(window).width() < 768) {
		setImage();
	}else {
		$('.MaterialPage').css({backgroundImage: 'none'})
	}	
});

$(window).resize(function() {
	if ( $(window).width() < 768) {
		setImage();
	}else {
		$('.MaterialPage').css({backgroundImage: 'none'})
	}
});

/*mobile menu*/
$('.burger').on('click', function() {

	if ($('line').attr('type') != 'close'){
		$(this).find('line').attr('type', 'close');
		$('.mobile-menu').css({
			transform: 'translateX(0)',
			transition : 'transform .5s ease-in-out'
		});
		setTimeout(function(){
			$('.mobile-menu').css({
			transform: 'translateX(0)',
			transition : 'transform 0s ease-in-out'
		});
		},20);
		$('html,body').css({
			overflowY: 'hidden'
		});
	} else {
		$(this).find('line').attr('type', 'burger');
		$('.mobile-menu').css({
			transform: 'translateX(-120%)',
			transition : 'transform .5s ease-in-out'
		});
		$('html,body').css({
			overflowY: 'auto'
		})
	}
});




/*choose lang*/
var clickLang = false;

$('.lang span').click(function(event) {
	
	if(!clickLang) {
		$('.choose-lang').show();
		clickLang = !clickLang;
		
	} else {
		$('.choose-lang').hide();
		clickLang = !clickLang;
	}
});

$(document).on('mouseup touchstart', function (e) {
	var langBlock = $(".lang");

	if (!langBlock.is(e.target) && langBlock.has(e.target).length === 0) {
		$('.choose-lang').hide();
		clickLang = false;
	}
});
/*end choose lang*/

function is_touch_device() {
	return (('ontouchstart' in window)
		|| (navigator.MaxTouchPoints > 0)
		|| (navigator.msMaxTouchPoints > 0));
}

if (is_touch_device()) {
	$('html').addClass('touch');
} else {
	$('html').addClass('no-touch');
}

$("#Tel").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter, +, F5
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 107, 116]) !== -1 ||
             // Allow: Ctrl+A, Command+A
             (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
             (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
             return;
         }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 107)) {
        	e.preventDefault();
        }
    });


$(window).scroll(function() {
	if($('.we-in-social').length) {


		var st 	 	= $(window).scrollTop();
		var $win 	= $(window);
		var $height = $win.height();

		if(st > ( 0.5*$height - $('header').height()) && st < 1.2*$height) {
			$('.transAnim p').css({
				transform: 'matrix(1, 0, 0, 1, 1, 0)',
				opacity: 1
			});
			$('.social').css({
				transform: 'scale(1)'
			});
			$('.we-in-social .title').css({
				opacity: 1
			})
			// console.log('downscroll')
		} else if (st >= 1.2*$height) {
			$('.transAnim p').css({
				transform: 'matrix(1, 0, 0, 1, 100, 0)',
				opacity: 0
			});
			// console.log('top')
		} else {
			$('.transAnim p').css({
				transform: 'matrix(1, 0, 0, 1, -100, 0)',
				opacity: 0
			});
		}
	}
});


$('.group input').on({
	focus: function() {
		var lbW = $(this).parent().find('label').width();
		if ($(window).width()>992) {
			$(this).parent().find('label').css({
				left: -lbW + (-15) + 'px',
				top: '6px'
			});
		} else {
			$(this).parent().find('label').css({
				left: '15px',
				top: '-15px'
			});
		}
		
	},
	focusout: function() {
		if (!this.value) {
			$(this).parent().find('label').css({
				top: '6px',
				left: '15px'
			});
		}
	}
});


$('select').on({
	focus: function() {
		var lbW = $(this).parent().siblings('label').width();

		if($(window).width()>992) {
			$(this).parent().siblings('label').css({
				left: -lbW + (-15) + 'px',
				top: '6px'
			});
		} else {
			$(this).parent().siblings('label').css({
				top: '-15px',
				left: '15px'
			});
		}

		$(this).addClass('focus');
	},
	change: function() {
		$(this).removeClass('focus');
		if ($(this).val() === "") {
			if ($(window).width()>992) {
				$(this).parent().siblings('label').css({
					left: '15px',
					top: '6px'
				});
			}else {
				$(this).parent().siblings('label').css({
					top: '6px',
					left: '15px'
				});
			}
			
		}
	},
	focusout: function(){
		if ($(this).val() === null) {
			if ($(window).width()>992) {
				$(this).parent().siblings('label').css({
					left: '15px'
				});
			}else {
				$(this).parent().siblings('label').css({
					top: '6px'
				});
			}
			
		};
		$(this).removeClass('focus');
	}
})


/*basket Count*/

$('.quantity').each(function(index, el) {
	var _this 	= $(this, el);

	_this.find('.minVal').click(function(){
		var dataPrice 	= $(this).attr('data-price');
		var val 		= _this.find('.counter').text();
		dataPrice	= parseInt(dataPrice);
		val 		= parseInt(val);
		val 		= val - 1;
		if(val < 1) {
			val = 1;
		}
		dataPrice	= parseInt(dataPrice);
		_this.find('.counter').text(val);
		dataPrice = dataPrice*val;
		$(this).parent().next('.totPrice').find('.product-total-price').text(dataPrice);

		totalPriceCounter();

	});

	_this.find('.maxVal').click(function(){
		var dataPrice 	= $(this).attr('data-price');
		dataPrice	= parseInt(dataPrice);
		var valM = _this.find('.counter').text();
		valM = parseInt(valM);
		valM +=  1;
		dataPrice = dataPrice*valM;
		_this.find('.counter').text(valM);
		$(this).parent().next('.totPrice').find('.product-total-price').text(dataPrice);

		totalPriceCounter();

	})


});


function totalPriceCounter(){
	var total_price = 0;
	$(".product-total-price").each(function() {
		total_price += parseInt($(this).text());
	});

	$("#product-total-price").text(total_price);

	total_price += parseInt($("#gift-price").text());

	$(".counter-total-price").text(total_price);
}


function setWidthLine() {
	var w = $('.reg').offset().left - $('.delivery').offset().left + 15;
	
	$('.lineHidden').css({
		width: -w + 'px'
	})
}

jQuery(window).load(function() {
	if($('.delivery').length || $('.reg').length) {
		setWidthLine();
	}
});
jQuery(window).resize(function() {
	if($('.delivery').length || $('.reg').length) {
		setWidthLine();
	}
});


jQuery(document).ready(function($) {
	if($('.reg').hasClass('active')) {
		$('.lineHidden-first').css({
			background: '#0cf'
		});
		$('.regTab').show();
	}

	if($('.delivery').hasClass('active')) {
		$('.lineHidden-first').css({
			background: '#0cf'
		});
		$('.lineHidden-last').css({
			background: '#0cf'
		})
		$('.deliveryTab').show();
	}

	if($('.payment').hasClass('active')) {

		$('.lineHidden-last').css({
			background: '#0cf'
		});
		$('.paymentTab').show();
	}


	$('#bankCard').change( function() {		
		$('.paymentTab .group').show();
		$('.payBtn').hide();
		$('.payGroup').show();
	});
	if($('#receive').prop('checked') == false) {
		$('.paymentTab .group').show();
		$('.payBtn').hide();
		$('.payGroup').show();
	} else {
		$('.paymentTab .group').hide();
		$('.payBtn').show();
		$('.payGroup').hide();
	}
	$('#receive').change( function() {
		if($(this).prop('checked') == false) {
			$('.paymentTab .group').show();
			$('.payBtn').hide();
			$('.payGroup').show();
		} else {
			$('.paymentTab .group').hide();
			$('.payBtn').show();
			$('.payGroup').hide();
		}
	});
	$('.no-touch  body').niceScroll({
		cursorcolor: "#666",
		cursorwidth: '5px',
		bouncescroll: true,
		cursorborderradius: 0,
		zindex: 99999,
		touchbehavior: false,
		horizrailenabled:false,
		preventmultitouchscrolling: false,
		cursordragontouch: true
	});


});


function succHeight() {	
	$('.success').css({
		minHeight: $(window).height() - $('header').innerHeight()
	})	
}

jQuery(document).ready(function($) {
	if($('.success').length) {
		succHeight();
	}
});
jQuery(window).resize(function($) {
	if(jQuery('.success').length) {
		succHeight();
	}
});
// var timerStart = Date.now();
// $(document).ready(function() {
// 	console.log("Time until DOMready: ", Date.now()-timerStart);
// });
$(window).load(function() {
	setTimeout(function(){
		$('.preloader').fadeOut(1000)
	},1500)
});