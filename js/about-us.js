$(document).ready(function(){

    $('.teamSlider')
        .slick({

            centerMode: true,
            centerPadding: '0',
            slidesToShow: 5,
            slidesToScroll: 3,
            focusOnSelect: true,
            arrows: true,
            infinite: true,
            draggable: false,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        centerMode: true,
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 1370,
                    settings: {
                        centerMode: true,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '0',
                        slidesToShow: 1
                    }
                }
            ]
        })
        .on('beforeChange', function(event, slick, currentSlide, nextSlide){
            removeSliderAttr();
        })
        .on('afterChange', function(event, slick, currentSlide, nextSlide){
            animateLinesSlider();
            slickHover();
        });



    slickHover();

    function is_touch_device() {
        return (('ontouchstart' in window)
        || (navigator.MaxTouchPoints > 0)
        || (navigator.msMaxTouchPoints > 0));
    }

    if (!is_touch_device()) {
        $('html').addClass('no-touch');
    }
    function slickHover() {
        $('.no-touch .slick-slide img').on({
            mouseover: function(){
                var src = $(this).attr('src');
                var mouseoverSrc = $(this).data('mouseover');
                $(this).attr('src', mouseoverSrc);
            },
            mouseout:function(){
                var src = $(this).attr('src');
                var mouseoutSrc = $(this).data('mouseout');
                $(this).attr('src', mouseoutSrc);
            }
        })
    }
    $('[data-modal-open]').click(function(e){
        e.preventDefault();
        $('.dialog').removeClass('dialog--close');
        $('.dialog').addClass('dialog--open');
    })
    $('.action').click(function(){
        $('.dialog').removeClass('dialog--open');
        $('.dialog').addClass('dialog--close');
    })
    $('.dialog__overlay').click(function(){
        $('.dialog').removeClass('dialog--open');
        $('.dialog').addClass('dialog--close');
    })

});


jQuery(document).ready(animateLinesSlider);
jQuery(window).resize(animateLinesSlider);

function animateLinesSlider(){
    removeSliderAttr();
    var ww = $(window).width();

    if(ww>991) {
        $( ".slick-current .line_3" ).addClass('trans').animate( 400,  function(){
            $( ".slick-current .line_2" ).addClass('trans').animate(  400, function() {
                $( ".slick-current .line_1" ).addClass('trans').animate(  400, function() {
                    $( ".slick-current .sliderTitle" ).addClass('trans');
                });
            });
        });
    } else {
        $( ".slick-current .sliderTitle" ).addClass('trans');
    }

};

function removeSliderAttr() {
    $('.trans').removeClass('trans')
}

