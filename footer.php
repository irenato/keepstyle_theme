<?php
/**
 * The template for displaying the footer
 *
 *
 * @package WordPress
 * @subpackage Keep Style
 * @since Keep Style 1.0
 */
?>

<!-- footer -->
<footer>
    <!-- footer-nav	 -->
    <div class="footer-nav">
        <div class="container">
            <div class="row visible-lg visible-md">

                <div class="col-lg-3 col-md-3">
                    <div href='#'  class="title">поддержка</div>
                    <p><?= get_field('footer_delivery_text_'.LANG_CODE, 'option') ?></p>
                    <a href="<?= '/'.LANG_CODE.'/help/' ?>"> Читать далее...</a>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div href='#'  class="title">FAQ</div>
                    <p><?= get_field('footer_return_text_'.LANG_CODE, 'option') ?></p>
                    <a href="<?= '/'.LANG_CODE.'/faq/' ?>"> Читать далее...</a>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div href='#'  class="title">подарочные карты</div>
                    <p><?= get_field('footer_faq_text_'.LANG_CODE, 'option') ?></p>
                    <a href="<?= '/'.LANG_CODE.'/gift-cards/' ?>"> Читать далее...</a>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div href='#'  class="title">Контакты</div>
                    <div class="contacts">
                        <p>Адресс: <a target="_blank" href="https://www.google.com/maps/place/%D0%BF%D0%BB.+%D0%9F%D0%BE%D0%B2%D1%81%D1%82%D0%B0%D0%BD%D0%BD%D1%8F,+7%2F8,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2,+%D0%A5%D0%B0%D1%80%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0/@49.9864487,36.2565887,17z/data=!3m1!4b1!4m5!3m4!1s0x4127a09056461c7f:0x28b0b12c6d711392!8m2!3d49.9864487!4d36.2587774"><?= get_field('footer_contacts_text_'.LANG_CODE, 'option') ?></a></p>
                        <div class='tel'>
                            <span>Телефон: </span>
                            <ul>
                                <li><a href="tel:<?= get_field('phone_number_1', 'option') ?>"><?= get_field('phone_number_1', 'option') ?></a></li>
                                <li><a href="tel:<?= get_field('phone_number_2', 'option') ?>"><?= get_field('phone_number_2', 'option') ?></a></li>
                            </ul>

                        </div>
                    </div>

                </div>
            </div>

            <div class="row visible-sm visible-xs">
                <ul class="foo-nav">
                    <li><a href="<?= '/'.LANG_CODE.'/help/' ?>">поддержка</a></li>
                    <li><a href="<?= '/'.LANG_CODE.'/faq/' ?>">FAQ</a></li>
                    <li><a href="<?= '/'.LANG_CODE.'/gift-cards/' ?>">подарочные карты</a></li>
                    <li><a href="<?= '/'.LANG_CODE.'/about_us/' ?>">О нас</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- footer-nav	 -->
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-12col-xs-12 ">
                <div class="title">Обратный звонок</div>

                <form class="callBack">
                    <div class="inputWrap">
                        <div class="inputParent">
                            <input type=text id="Name" placeholder="Имя" name="name">
                            <!--   <label for="name">Name</label> -->
                            <span class="bot-line"></span>
                            <span class="right-line"></span>
                        </div>

                        <div class="inputParent">
                            <input type="text" id="Tel" placeholder="Телефон"  name="phone">
                            <!--   <label for="name">Name</label> -->
                            <span class="bot-line"></span>
                            <span class="right-line"></span>
                        </div>
                    </div>

                    <input type="submit" value="Заказать звонок">
                </form>
            </div>
            <div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-6 col-xs-6 social">
                <div class="title">Социальные сети</div>
                <ul>
                    <li><a class='fb' href="<?= get_field('facebook_link', 'option') ?>" target="_blank"></a></li>
                    <li><a class='vk' href="<?= get_field('vkontakte_link', 'option') ?>" target="_blank"></a></li>
                    <li><a class='insta' href="<?= get_field('instagram_link', 'option') ?>" target="_blank"></a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-lg-offset-1 col-md-4 col-sm-6 col-xs-6 payment">
                <div class="title">платежные системы</div>
                <ul>
                    <li><img src="<?= THEMROOT ?>/img/mc.png" alt="Mastercard"></li>
                    <li><img src="<?= THEMROOT ?>/img/pp.png" alt="Paypal"></li>
                    <li><img src="<?= THEMROOT ?>/img/visa.png" alt="Visa"></li>
                </ul>
            </div>


        </div>

        <div class="row copyright">
            <div class="col-lg-12">
                <p>Copyright © <a href="<?= get_home_url() ?>"><?= get_bloginfo('name') ?></a> 2015 - <?= date("Y") ?></p>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->
<?php if(is_page(2) || is_page(8)): ?>
<div class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content">
        <div class="dialogWrapper">
            <div class="title">Оставить отзыв<div class="blueSmallLine"></div></div>

            <div id="dropzone">
                <div class="dropzone_title">
                    <i class="icon-camera"></i>
                    <br>
                    Загрузить свое фото
                </div>
                <input type="file" accept="image/*" />
            </div>
            <div class="group">
                <input type="text" name="first-name" class="current-value">
                <span class="bot-line"></span>
                <span class="right-line"></span>
                <label >Имя</label>
            </div>
            <div class="inputArea">
                <textarea placeholder='Отзыв...'></textarea>
                <input type="submit" data-id="<?= get_the_ID() ?>" data-image="" value="Отправить">
            </div>
        </div>

        <i class="action icon-close" data-dialog-close></i>

    </div>
</div>
<?php endif; ?>

<?php if(is_page(11) || is_page(13)): ?>
<div class="modalAdult">
    <div class="icon-close"></div>
    <img src="<?= THEMROOT ?>/img/adultSize.jpg" alt="">
</div>
<div class="modalChild">
    <div class="icon-close"></div>
    <img src="<?= THEMROOT ?>/img/childSize.jpg" alt="">
</div>
<?php endif; ?>
<!-- script -->
<?php wp_footer(); ?>
<!-- <script defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> -->

</body>
</html>
