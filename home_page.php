<body>

<!--<div class="preloader">-->
<!--    <div class="preloader_overlay"></div>-->
<!--    <div class="preloader_content">-->
<!--        <img src="--><?//= THEMROOT ?><!--/img/logoHeader.png" alt="Logo">-->
<!--    </div>-->
<!--</div>-->

<div class="mobile-menu">
    <?php wp_nav_menu(array(
        'theme_location'=> 'menuMobile',
    )); ?>
</div>


<!-- mobile-btn -->
	<span class='mobile-button'>
		<svg class="burger" width="30px" height="30px">
            <line class="top" type="burger" x1="0" y1="1" x2="30" y2="1" />
            <line class="middle" type="burger" x1="0" y1="11" x2="30" y2="11" />
            <line class="bottom" type="burger" x1="0" y1="22" x2="30" y2="22" />
        </svg>
	</span>
<!-- mobile-btn -->


<!-- header -->
<header>

    <div class="container">
        <div class="row">

            <nav class='hidden-xs'>
                <?php wp_nav_menu(array(
                    'theme_location'=> 'menu',
                )); ?>
            </nav>
            <a href="<?= get_home_url() ?>" class="logo">
                <img src="<?= THEMROOT ?>/img/logoHeader.png" alt="">
            </a>
            <div class="right-side">
                <div class="lang">
                    <?php keep_style_language_switcher(); ?>
                </div><!-- lang  -->
                <div class="cart hidden-xs">
                    <a href="<?= '/'.LANG_CODE.'/checkout/' ?>">корзина (<span class="count"><?= ACWProducts::getCount() ?></span>)</a>
                </div>
            </div>
        </div>
    </div>



</header>
<!-- header -->


<!-- headSite -->
<div class="headSite">
    <div class="container">
        <div class="row tag">
            <div class="logo col-lg-4 col-lg-offset-1 col-md-10 col-md-offset-1	">
                <a href="<?= get_home_url() ?>"><img src="<?= THEMROOT ?>/img/logo.png" alt="Keepstyle Logo"></a>
            </div>
            <div class="tagline col-lg-5 col-lg-offset-1 col-md-10  col-md-offset-1 ">
                <p class='hidden-smart'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quam sem, ultricies id malesuada eget, aliquet quis metus. Cras in ipsum non ipsum dignissim dapibus. Praesent eget massa porttitor, molestie dolor vitae, efficitur metus. Suspendisse sit amet lorem et eros commodo condimentum.</p>
            </div>
        </div>
    </div>


    <div id="scrollDown" class='scrollDown'>
        <i class='icon-scroll'></i>
        <p>scroll down</p>
    </div>
</div>
<!-- headSite -->

<!-- we-in-social -->
<div class="we-in-social">
    <div class="container">
        <div class="row transAnim">
            <div class="title">Мы в социальных сетях</div>
        </div>

        <div class="row transAnim">
            <div class="col-lg-3 col-md-4 col-lg-offset-1 col-sm-4 col-xs-4">
                <a class='fb social' href="<?= get_field('facebook_link', 'option') ?>" target="_blank"></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <a class='vk social' href="<?= get_field('vkontakte_link', 'option') ?>" target="_blank"></a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                <a class='insta social' href="<?= get_field('instagram_link', 'option') ?>" target="_blank"></a>
            </div>
        </div>

        <div class="row transAnim">
            <div class="col-lg-10 col-lg-offset-1">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. </p>
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-10 col-lg-offset-1 slideImage">
                <img src="<?= THEMROOT ?>/img/weInSocial.jpg" alt="We in social">
                <div class="slideTitle">
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. </span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- we-in-social -->

<div class="BeoMaterials " data-bg="#fff" data-font="#333" data-snap="yes" data-useCircle="no" data-anchor="materials" logo-colour="#fff" data-align="right" >
    <div class="MaterialPage" data-bg="#dfdfdf" data-font="#333"
         data-img="<?= THEMROOT ?>/img/1.jpg" data-icon="<?= THEMROOT ?>/img/icon1.png">
        <div class="text">
            <h4>футболки</h4>
            <div class="materialbreadtext">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Curabitur in libero ipsum. Morbi orci risus, vulputate id lacinia ut, rutrum eget nibh.
            </div>
            <a href="<?= '/'.LANG_CODE.'/create?1' ?>" class='see-product'>смотреть</a>
        </div>
    </div>

    <div class="MaterialPage" data-bg="#d2d2d2" data-font="#333"	data-img="<?= THEMROOT ?>/img/2.jpg" data-icon="<?= THEMROOT ?>/img/icon2.png">
        <div class="text">
            <h4>свитшоты</h4>
            <div class="materialbreadtext">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Curabitur in libero ipsum. Morbi orci risus, vulputate id lacinia ut, rutrum eget nibh.
            </div>
            <a href="<?= '/'.LANG_CODE.'/create' ?>" class='see-product'>смотреть</a>
        </div>
    </div>

    <div class="MaterialPage" data-bg="#bfbfbf" data-font="#333" data-img="<?= THEMROOT ?>/img/3.jpg" data-icon="<?= THEMROOT ?>/img/icon3.png">
        <div class="text">
            <h4>поло</h4>
            <div class="materialbreadtext">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Curabitur in libero ipsum. Morbi orci risus, vulputate id lacinia ut, rutrum eget nibh.
            </div>
            <a href="<?= '/'.LANG_CODE.'/create' ?>" class='see-product'>смотреть</a>
        </div>
    </div>



    <div class="MaterialPage" data-bg="#b4e5fc" data-font="#333" data-img="<?= THEMROOT ?>/img/4.jpg" data-icon="<?= THEMROOT ?>/img/icon4.png">
        <div class="text">
            <h4>комбинезоны</h4>
            <div class="materialbreadtext">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Curabitur in libero ipsum. Morbi orci risus, vulputate id lacinia ut, rutrum eget nibh.
            </div>
            <a href="<?= '/'.LANG_CODE.'/create' ?>" class='see-product'>смотреть</a>
        </div>
    </div>

    <div class="MaterialPage" data-bg="#c9ebfb" data-font="#333" data-img="<?= THEMROOT ?>/img/5.jpg" data-icon="<?= THEMROOT ?>/img/icon5.png">
        <div class="text">
            <h4>боди</h4>
            <div class="materialbreadtext">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Curabitur in libero ipsum. Morbi orci risus, vulputate id lacinia ut, rutrum eget nibh.
            </div>
            <a href="<?= '/'.LANG_CODE.'/create' ?>" class='see-product'>смотреть</a>
        </div>
    </div>

    <div class="MaterialPage" data-bg="#e6f3f9" data-font="#333" data-img="<?= THEMROOT ?>/img/6.jpg" data-icon="<?= THEMROOT ?>/img/icon6.png">
        <div class="text">
            <h4>Кеды</h4>
            <div class="materialbreadtext">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque risus dui, rutrum ac maximus a, volutpat eu elit. Curabitur in libero ipsum. Morbi orci risus, vulputate id lacinia ut, rutrum eget nibh.
            </div>
            <a href="<?= '/'.LANG_CODE.'/create' ?>" class='see-product'>смотреть</a>
        </div>

    </div>
</div>




<!-- Our image to be blurred -->
<img data-canvas-image style="display: none" src="<?= THEMROOT ?>/img/6.jpg" />
<!-- insta-gallery -->
<div class="grid insta-gallery">
    <div class="grid-sizer"></div>	<!-- width of .grid-sizer used for columnWidth -->

    <div class="grid-item" id="image-grid-item-1"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/7.png" alt=""></a></div>
    <div class="grid-item" id="image-grid-item-2"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/1.png" alt=""></a></div>

    <div class="grid-item grid-item--withTxt">
        <i class='icon-inst'></i>
        <span class='line'></span>
        <p>«Vestibulum sem metus, dictum ut turpis quis, viverra malesuada mauris.» </p>
        <a class='subscribe' target='_blank' href="https://www.instagram.com/keep_style/">Подписаться</a>
        <a  class='hash'>#keepstyleshop</a>
        <a class="clients">Наши клиенты</a>
    </div>

    <div class="grid-item " id="image-grid-item-9"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/2.png" alt=""></a></div>
    <div class="grid-item" id="image-grid-item-3"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/5.png" alt=""></a></div>
    <div class="grid-item" id="image-grid-item-4"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/4.png" alt=""></a></div>
    <div class="grid-item" id="image-grid-item-5"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/3.png" alt=""></a></div>
    <div class="grid-item" id="image-grid-item-6"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/9.png" alt=""></a></div>
    <div class="grid-item" id="image-grid-item-7"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/6.png" alt=""></a></div>
    <div class="grid-item" id="image-grid-item-8"><a href="" target="_blank"><img src="<?= THEMROOT ?>/img/insta/7.png" alt=""></a></div>

</div>
<!-- insta-gallery -->

